package com.jianye.recycdona.model.fragment.find.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jianye.recycdona.R;
import com.jianye.recycdona.model.fragment.find.CommentActivity;
import com.jianye.recycdona.model.fragment.find.bean.Find;

import java.util.List;

public class FindAllListAdapter extends BaseQuickAdapter<Find.ContentBean, FindViewHolder> {

    List<Find.ContentBean> findall_list;

    public void setContext(Context context) {
        this.context = context;
    }

    Context context;

    public FindAllListAdapter(int layoutResId, @Nullable List<Find.ContentBean> data) {
        super(layoutResId, data);
        this.findall_list=data;
    }


    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void convert(FindViewHolder helper, Find.ContentBean item) {
        helper.icon.setImageResource(R.drawable.logo);

        helper.name.setText(findall_list.get(helper.getAdapterPosition()).getFname());
        helper.decl.setText(findall_list.get(helper.getAdapterPosition()).getDecl());
        helper.lable.setText(findall_list.get(helper.getAdapterPosition()).getLabel());
        helper.content.setText(findall_list.get(helper.getAdapterPosition()).getContent());
        String[] data=findall_list.get(helper.getAdapterPosition()).getCreateDate().split(" ");
        helper.find_data.setText(data[0]);
        helper.comment_number.setText(findall_list.get(helper.getAdapterPosition()).getCommentnumber()+"");

        for (Find.ContentBean contentBean:findall_list) {
            Log.d(TAG, "getView: "+contentBean.getLabel());
        }

        ImageView[] imageViews=new ImageView[]{helper.find_image1,helper.find_image2
                ,helper.find_image3,helper.find_image4,helper.find_image5,helper.find_image6};

        Log.d(TAG, "getView: "+findall_list.size());
        String[] arry=findall_list.get(helper.getAdapterPosition()).getImages().split(",");
        int imglength=arry.length;
        if (imglength>1){
            for (int i=0;i<imglength;i++){
                Glide.with(context).load(arry[i]).into(imageViews[i]);
                imageViews[i].setVisibility(View.VISIBLE);
            }
        }
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (imglength==1){
            Glide.with(context).load(arry[0]).into(helper.one_img);
            helper.one_img.setVisibility(View.VISIBLE);
        }


        helper.Commentcomtent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, CommentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id",findall_list.get(helper.getAdapterPosition()).getId()+"");
                intent.putExtra("name",findall_list.get(helper.getAdapterPosition()).getFname());
                intent.putExtra("decl",findall_list.get(helper.getAdapterPosition()).getDecl());
                intent.putExtra("lable",findall_list.get(helper.getAdapterPosition()).getLabel());
                intent.putExtra("content",findall_list.get(helper.getAdapterPosition()).getContent());
                intent.putExtra("images",findall_list.get(helper.getAdapterPosition()).getImages());
                intent.putExtra("find_data",findall_list.get(helper.getAdapterPosition()).getCreateDate());
                intent.putExtra("commentnumber",findall_list.get(helper.getAdapterPosition()).getCommentnumber()+"");
                context.startActivity(intent);
            }
        });

    }






}
