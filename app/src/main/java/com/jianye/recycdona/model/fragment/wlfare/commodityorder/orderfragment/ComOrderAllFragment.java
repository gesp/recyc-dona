package com.jianye.recycdona.model.fragment.wlfare.commodityorder.orderfragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.ComOrder;
import com.jianye.recycdona.model.fragment.wlfare.adapter.ComOrderAdapter;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.FragCommodityOderActivity;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;


/**
 * 全部
 */
public class ComOrderAllFragment extends Fragment implements AdapterView.OnItemClickListener , View.OnClickListener{


     ListView listViewAll;
     List<ComOrder.DataBean> list;


    private   View view;
    private ComOrderAdapter commoditAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view==null){
            view=inflater.inflate( R.layout.fragment_com_order_all, container, false);
        }

        initData();   //加载数据，初始化数据，初始化对象

        return view;
    }

    public void initData() {

        listViewAll =view.findViewById(R.id.comorder_listview);
        list=new ArrayList<>();
        //适配数据
        list= FragCommodityOderActivity.list;

        for (ComOrder.DataBean c:list) {
            Log.d(TAG, "loadingDatas: "+c.getStatus());
        }

        //倒序
//        Collections.reverse(list);
        //设置适配器
       commoditAdapter=new ComOrderAdapter(list,getContext());
        listViewAll.setAdapter(commoditAdapter);
        listViewAll.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Log.d(TAG, "onItemClick: --------->position"+position);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.confirmreceipt:
                commoditAdapter.notifyDataSetChanged();
                break;
        }
    }
}