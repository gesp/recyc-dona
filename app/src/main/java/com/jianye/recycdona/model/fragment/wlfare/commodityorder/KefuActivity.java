package com.jianye.recycdona.model.fragment.wlfare.commodityorder;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.jianye.recycdona.R;

public class KefuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kefu);

        ActionBar actionBar=getSupportActionBar();

        if (actionBar!=null){
            actionBar.hide();
        }
    }

    public void Finish(View view) {
        finish();
    }

    public void callphone(View view) {

        if (ContextCompat.checkSelfPermission(KefuActivity.this, Manifest.permission.CALL_PHONE)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(KefuActivity.this,new String[]{Manifest.permission.CALL_PHONE},1);
        }else {
            Dialog();


        }


    }

    public void Dialog(){

        AlertDialog.Builder normalMoreButtonDialog = new AlertDialog.Builder(KefuActivity.this);
        normalMoreButtonDialog.setTitle("提示");
        normalMoreButtonDialog.setMessage("确定拨打电话"+"18867244743"+"给客服？");
        //设置按钮
        normalMoreButtonDialog.setPositiveButton("确定"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        try {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_CALL);
                            //设置拨打电话的号码-->
                            intent.setData(Uri.parse("tel:18867244743"));
                            //开启打电话的意图-->
                            startActivity(intent);

                        }catch (SecurityException e){
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });

        normalMoreButtonDialog.setNeutralButton("取消"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });



        normalMoreButtonDialog.create().show();
    }
}