package com.jianye.recycdona.model.fragment.wlfare.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jianye.recycdona.control.bean.CommoditOrder;

import java.util.ArrayList;
import java.util.List;


/**
 * 商品列表显示适配
 */
public class CommoditAdapter extends BaseQuickAdapter<CommoditOrder.ContentBean,ComViewHolder> {

    List<CommoditOrder.ContentBean> list=new ArrayList<>();

    public void setContext(Context context) {
        this.context = context;
    }

    Context context;

    public CommoditAdapter(int layoutResId, @Nullable List<CommoditOrder.ContentBean> data) {
        super(layoutResId, data);
        this.list=data;
    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void convert(ComViewHolder helper, CommoditOrder.ContentBean item) {

            Glide.with(context).load(list.get(helper.getAdapterPosition()).getProduconver()).into( helper.commodit_imamge);
            helper.commdity_name.setText(list.get(helper.getAdapterPosition()).getProductName());
            helper.commdity_baoyou.setText(list.get(helper.getAdapterPosition()).getFreigh());
            helper.commodiy_price.setText("¥ "+list.get(helper.getAdapterPosition()).getPrice()+"");
            Log.d("convert", "-------------------->convert: "+list.get(helper.getAdapterPosition()).getProductName()+"");

    }



}
