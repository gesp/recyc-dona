package com.jianye.recycdona.model.fragment.wlfare.commodityorder;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.jianye.recycdona.R;
import com.jianye.recycdona.control.http.CommoditOrderHttp;
import com.jianye.recycdona.model.fragment.home.HuishouActivity;

import java.util.Date;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;

public class ComOrderDetailsTowActivity extends AppCompatActivity {

    private TextView comorderdetail_name,comorderdetail_comname,comorder_yunfei;
    ImageView comorderDetail_image;
    private TextView comorderdetail_address,comorder_date,ordernember,comorder_sprice,comorder_guige,comorder_price,comorder_number;
    private TextView tv_detail_reserveStatus,commodit_kefu,commdity_fankui;
    LinearLayout order_cotainel;
    private String orderid;
    private String userid;
    private String username;
    private String orderinfo;
    private String[] nameArray;
    private String orderinfoCOM;
    private String thisData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_com_order_details_tow);

        ActionBar actionBar=getSupportActionBar();

        if (actionBar!=null){
            actionBar.hide();
        }
        initFindViewById();
        initData();
    }
    @SuppressLint("WrongViewCast")
    private void initFindViewById() {
        tv_detail_reserveStatus=findViewById(R.id.tv_detail_reserveStatus);
        comorderdetail_name=findViewById(R.id.comorderdetail_name);
        comorderdetail_address=findViewById(R.id.comorderdetail_address);
        comorderDetail_image=findViewById(R.id.comorderdetail_image);
        comorderdetail_comname=findViewById(R.id.comorderdetail_comname);
        comorder_guige=findViewById(R.id.comorder_guige);
        comorder_price=findViewById(R.id.comorder_price);
        comorder_number=findViewById(R.id.comorder_number);
        comorder_yunfei=findViewById(R.id.comorder_yunfei);
        comorder_sprice=findViewById(R.id.comorder_sprice);
        ordernember=findViewById(R.id.ordernember);
        comorder_date=findViewById(R.id.comorder_date);
        commodit_kefu=findViewById(R.id.order_kefu);
        commdity_fankui=findViewById(R.id.commody_fangkui);
        order_cotainel=findViewById(R.id.order_cotainel);
    }
    @SuppressLint("SetTextI18n")
    private void initData() {
        Intent intent=getIntent();
        //订单id
         orderid=intent.getStringExtra("orderid");
        SharedPreferences sp = getSharedPreferences("config", MODE_PRIVATE);//confi 文件名
        //用户id
        userid=sp.getString("id","");
        //用户名
        username=sp.getString("username","");

        String orderinfoadd=intent.getStringExtra("addressinfo");
        assert orderinfoadd != null;
        String[] addressinfoArray=orderinfoadd.split("\n");
        for (String s:addressinfoArray) {
            Log.e(TAG, "initData: "+s);
        }
        comorderdetail_name.setText(addressinfoArray[0]);
        comorderdetail_address.setText(addressinfoArray[1]);

        String status=intent.getStringExtra("Status");
        //显示
        if (status.equals("交易完成")){
            commdity_fankui.setVisibility(View.VISIBLE);
        }
        tv_detail_reserveStatus.setText(status);

        //订单信息
        orderinfo=intent.getStringExtra("orderinfo");
        assert orderinfo != null;
        String[] orderinfoArray=orderinfo.split("\n");
        for (String s:orderinfoArray) {
            Log.d(TAG, "initData: orderarry"+s);
        }
        Glide.with(ComOrderDetailsTowActivity.this).load(orderinfoArray[2]).into(comorderDetail_image);
        //价格
        comorder_price.setText(orderinfoArray[1]+"元");
        //name
        nameArray=orderinfoArray[0].split(",");
        comorderdetail_comname.setText(nameArray[0]);
        comorder_guige.setText("规格："+nameArray[1]);
        //当前时间
        thisData= new HuishouActivity().getTime(new Date());

        comorder_number.setText(intent.getStringExtra("number"));
        comorder_yunfei.setText(intent.getStringExtra("feight"));
        comorder_date.setText(intent.getStringExtra("orderTime"));
        ordernember.setText(intent.getStringExtra("ordernum"));
        String sprice="支付"+intent.getStringExtra("paidpoint")+"积分\t\t"+"原价"+intent.getStringExtra("price");
        comorder_sprice.setText(sprice);


        //商品信息
        orderinfoCOM="规格："+nameArray[1]+","+sprice+"数量"+intent.getStringExtra("number")+","+"订单号"+intent.getStringExtra("ordernum");



    }


    //客服
    public void order_kefu(View view) {
        startActivity(new Intent(ComOrderDetailsTowActivity.this,KefuActivity.class));
    }

    //反馈
    public void commody_fangkui(View view) {
        final EditText editText = new EditText(ComOrderDetailsTowActivity.this);
        AlertDialog.Builder inputDialog = new AlertDialog.Builder(ComOrderDetailsTowActivity.this);
        inputDialog.setTitle("请输入您的商品反馈内容").setView(editText);

        inputDialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String comtent = editText.getText().toString().trim();

                if (!comtent.equals("")) {
                   // userid, shopid, name, content, createdata,  shopobject,  orderinfo
                    try {
                        CommoditOrderHttp.getMyOkHttp().postShopCommentComment(userid,orderid,username,comtent,thisData,nameArray[0],orderinfoCOM);
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(ComOrderDetailsTowActivity.this, "已反馈", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ComOrderDetailsTowActivity.this, "内容不能不能为空", Toast.LENGTH_SHORT).show();
                }

            }
        }).show();
    }

    public void Finish(View view) {
        finish();
    }
}