package com.jianye.recycdona.model.fragment.find.adapter;

import android.content.Context;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jianye.recycdona.model.fragment.find.bean.Comment;

import java.util.List;

public class CommentAdapter extends BaseQuickAdapter<Comment.DataBean, CommentHolder> {

    List<Comment.DataBean> list;

    public void setContext(Context context) {
        this.context = context;
    }

    Context context;

    public CommentAdapter(int layoutResId, @Nullable List<Comment.DataBean> data) {
        super(layoutResId, data);
        this.list=data;

    }

    @Override
    protected void convert(CommentHolder helper, Comment.DataBean item) {


        helper.name.setText(list.get(helper.getAdapterPosition()).getName());
        helper.content.setText(list.get(helper.getAdapterPosition()).getComentinfo());
    }
}
