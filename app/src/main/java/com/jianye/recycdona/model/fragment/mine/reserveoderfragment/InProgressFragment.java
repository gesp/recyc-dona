package com.jianye.recycdona.model.fragment.mine.reserveoderfragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.RecyclerOrder;
import com.jianye.recycdona.control.http.OrderHttp;
import com.jianye.recycdona.model.fragment.mine.listviewAapater.OrderAdapater;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.Context.MODE_PRIVATE;


public class InProgressFragment extends Fragment {


    View view;
    List<RecyclerOrder.DataBean> list = new ArrayList<>();;
    ListView listView;
    OrderAdapater orderAdapater;
    private SharedPreferences confg_sp;
    private String id;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_in_progress, container, false);
        }


        initFindViewId(0,null);

        return view;
    }

    //数据初始化
    public void initFindViewId(Integer updata, List<RecyclerOrder.DataBean> delist) {

        listView = view.findViewById(R.id.orderListview_InProgress);
        confg_sp = getActivity().getSharedPreferences("config", MODE_PRIVATE);//confi 用户数据文件

        if (list!=null)list.clear();

        if (updata==2&&delist!=null){
            for (int i=0;i<delist.size();i++){

                if (delist.get(i).getOrderStatus().equals("进行中")&&delist.get(i).getReserveStatus().equals("已取件")
                        ||delist.get(i).getReserveStatus().equals("等待取件")
                ){
                    list.add(delist.get(i));
                }

            }
            orderAdapater = new OrderAdapater(getActivity(), list,2);
            listView.setAdapter(orderAdapater);
        }else {


            id = confg_sp.getString("id", "");//父id
            Log.e("orderIDSelect", "------------------------------->" + id);

            try {
                List<RecyclerOrder.DataBean> orderInfo = OrderHttp.getMyOkHttp().getOrderInfo(id);//发起请求
                //过滤
                for (int i=0;i<orderInfo.size();i++){ //刷新

                    if (orderInfo.get(i).getOrderStatus().equals("进行中")&&orderInfo.get(i).getReserveStatus().equals("已取件")
                            ||orderInfo.get(i).getReserveStatus().equals("等待取件")
                    ){
                        list.add(orderInfo.get(i));
                    }

                }

                orderAdapater = new OrderAdapater(getActivity(), list,2);
                listView.setAdapter(orderAdapater);

            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }

        }




    }
}