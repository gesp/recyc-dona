package com.jianye.recycdona.model.fragment.find.bean;

import java.util.List;


public class Find {

    private List<ContentBean> content;
    private Integer totalElements;

    public List<ContentBean> getContent() {
        return content;
    }

    public void setContent(List<ContentBean> content) {
        this.content = content;
    }

    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    public static class ContentBean {
        private Integer commentnumber;
        private String content;
        private String createDate;
        private String decl;
        private String fname;
        private Integer id;
        private String images;
        private String label;
        private Integer thubsnumber;

        public Integer getCommentnumber() {
            return commentnumber;
        }

        public void setCommentnumber(Integer commentnumber) {
            this.commentnumber = commentnumber;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getDecl() {
            return decl;
        }

        public void setDecl(String decl) {
            this.decl = decl;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Integer getThubsnumber() {
            return thubsnumber;
        }

        public void setThubsnumber(Integer thubsnumber) {
            this.thubsnumber = thubsnumber;
        }
    }
}
