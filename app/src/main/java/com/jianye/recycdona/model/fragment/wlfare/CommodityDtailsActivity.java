package com.jianye.recycdona.model.fragment.wlfare;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.facebook.drawee.view.SimpleDraweeView;
import com.jianye.recycdona.R;
import com.jianye.recycdona.model.arryutils.ScreenUtil;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.FragCommodityOderActivity;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.bean.commodity;
import com.stx.xhb.xbanner.XBanner;
import com.stx.xhb.xbanner.transformers.Transformer;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class CommodityDtailsActivity extends AppCompatActivity {

    private XBanner mBanner;
    private ArrayList<commodity> comcarouseList;
    private TextView com_Exchangelimit;
    private TextView com_name;
    private TextView com_sold;
    private TextView com_baoyou;
    private TextView com_surplus;
    private TextView com_details;
    private TextView com_price;
    LinearLayout image_group;
    private ViewGroup llWindLayout;
    private String freigh;
    private String specifi;
    private String price;
    private String[] detals_inmage;
    private String name;
    private String[] comcarouseArry;
    private String pints_redeem;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_com_order_dtails);

        initFindViewId();
        initData();
        initBanner(mBanner);//Banner点击事件
    }


    private void initFindViewId() {
        image_group=findViewById(R.id.image_group);
        com_price = findViewById(R.id.com_price);//价格
        com_Exchangelimit=findViewById(R.id.com_Exchangelimit);//商品可兑换额度
        com_name=findViewById(R.id.com_name);//商品名称
        com_sold=findViewById(R.id.com_sold);//已售44件
        com_baoyou=findViewById(R.id.com_baoyou);//包邮
        com_surplus=findViewById(R.id.com_surplus);//剩余951件
        com_details=findViewById(R.id.com_details);//商品详情
    }


    /**
     * 初始化数据
     */
    @SuppressLint("SetTextI18n")
    private void initData() {

        comcarouseList = new ArrayList<>();
        Intent intent=getIntent();
        //商品名称
         name=intent.getStringExtra("name");
        com_name.setText(name);
        //商品已售
        String sold=intent.getStringExtra("sold");
        com_sold.setText("已售:"+sold);
        //商品id
         id=intent.getStringExtra("id");

        //切割轮播图
        String commcarousel=intent.getStringExtra("commcarousel");
        Log.d(TAG, "initData: "+commcarousel);
        assert commcarousel != null;
        comcarouseArry=commcarousel.split(",");
        for (String s : comcarouseArry) {
            comcarouseList.add(new commodity(s));
        }
        //商品规格
        specifi=intent.getStringExtra("specifi");
        String details=intent.getStringExtra("produdetails");
        assert details != null;
        String detai=details.replace("。","\n");
        com_details.setText("[产品规格]"+specifi+"\n"+detai);
        //运费
         freigh=intent.getStringExtra("freigh");
        com_baoyou.setText(freigh);
        //库存
        String insock=intent.getStringExtra("insock");
        com_surplus.setText("剩余"+insock+"件");
        //价格
        price=intent.getStringExtra("price");
        com_price.setText("¥ "+price);

        //图片详情
        String details_map=intent.getStringExtra("details_map");
         detals_inmage=details_map.split(",");
        addGroupImage(detals_inmage);//去设置图片
        //所需积分
        pints_redeem=intent.getStringExtra("pints_redeem");



        Log.e("商品名","name="+name+commcarousel);

        mBanner =findViewById(R.id.com_banner);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ScreenUtil.getScreenWidth(CommodityDtailsActivity.this) / 2);
        mBanner.setLayoutParams(layoutParams);
        mBanner.setPageTransformer(Transformer.Default);

        mBanner.setAutoPlayAble(comcarouseList.size() > 1);
        mBanner.setIsClipChildrenMode(true);
        mBanner.setBannerData(R.layout.layout_fresco_imageview, comcarouseList);
//        titles.add("这是第1张图片");
//        titles.add("第2张图片");
//        titles.add("这是第3张图片");
//        titles.add("这是第4张图片");
    }
    //解析商品图片详情，动态添加ImageView
    private void addGroupImage(String[] detals_inmage ) {
        image_group.removeAllViews();  //clear linearlayout
        for (String s : detals_inmage) {

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 0);
            ImageView imageView = new ImageView(this);
            //图片资源
            Glide.with(getApplicationContext()).load(s).into(imageView);

            imageView.setLayoutParams(lp);


            image_group.addView(imageView); //动态添加图片
        }
    }
    //初始化轮播图
    private void initBanner(XBanner mBanner) {

        //点击事件
        mBanner.setOnItemClickListener(new XBanner.OnItemClickListener() {
            @Override
            public void onItemClick(XBanner banner, Object model, View view, int position) {
                Toast.makeText(CommodityDtailsActivity.this, "点击了第" + (position + 1) + "图片", Toast.LENGTH_SHORT).show();
            }
        });
        //加载广告图片
        mBanner.loadImage(new XBanner.XBannerAdapter() {
            @Override
            public void loadBanner(XBanner banner, Object model, View view, int position) {
                //此处适用Fresco加载图片，可自行替换自己的图片加载框架
                SimpleDraweeView draweeView = (SimpleDraweeView) view;
                commodity listBean = (( commodity) model);
                String url = listBean.getCarousel();
                draweeView.setImageURI(Uri.parse(url));
            }
        });
    }

    public void Finish(View view) {
        finish();
    }

    public void ComstarActivity(View view) {
        startActivity(new Intent(CommodityDtailsActivity.this, FragCommodityOderActivity.class));
    }

    //发送订单到后台订单增加
    public void comCommoditOrder(View view) {

        Log.d(TAG, "comCommoditOrder: "+id);
        Intent intent=new Intent(CommodityDtailsActivity.this, ComOrderActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",id);
        intent.putExtra("name",name);//商品name
        intent.putExtra("specifi",specifi);//商品规格
        intent.putExtra("commcarousel",comcarouseArry[0]);//商品图
        intent.putExtra("price",price);//商品价格
        intent.putExtra("freigh",freigh);//运费
        intent.putExtra("pints_redeem",pints_redeem);//所需兑换积分
        startActivity(intent);
    }
}