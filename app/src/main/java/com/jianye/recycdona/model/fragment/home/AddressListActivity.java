package com.jianye.recycdona.model.fragment.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.jianye.recycdona.R;
import com.jianye.recycdona.control.http.ReserveHttp;
import com.jianye.recycdona.model.fragment.home.bean.AddressInfoAdapter;
import com.jianye.recycdona.model.fragment.home.bean.addressInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AddressListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView addressListView;
    AddressInfoAdapter addressListAdapter;
    List<addressInfo.DataBean> list;
    SharedPreferences sp,confg_sp;
    String parenId;



    @Override
    protected void onResume() {
        super.onResume();
        initSetData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//竖屏
        ActionBar actionBar=getSupportActionBar();

        if (actionBar!=null){
            actionBar.hide();
        }

        initFindVivewByID();//id初始化
        initSetData();//数据加载

    }

    //数据加载
    private void initSetData() {



        list.clear();
        try {
            //发起请求，查询用户的所有地址
           List<addressInfo.DataBean> addressData=ReserveHttp.getMyOkHttp().getAddressInfo(parenId,sp);

            Log.d("addressData-size", "------>?initSetData: "+addressData.size());
            list.addAll(addressData);

            addressListAdapter= new AddressInfoAdapter(list,getApplicationContext());
            addressListView.setAdapter(addressListAdapter);

        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    //id初始化
    private void initFindVivewByID() {

        addressListView=findViewById(R.id.addressList);
        list= new ArrayList<addressInfo.DataBean>();
        sp= getSharedPreferences("Address", MODE_PRIVATE);//Address 文件名
        confg_sp=getSharedPreferences("config", MODE_PRIVATE);//confi 用户数据文件
        parenId=confg_sp.getString("id","");

        addressListView.setOnItemClickListener(this);

    }


    //结束
    public void Finish(View view) {
        finish();
    }

    //添加地址
    public void addinfo(View view) {
        startActivity(new Intent(AddressListActivity.this,Add_addressInfoActivity.class));
    }

    /**
     * 2021.10.7
     * ListView 点击Item，选择地址信息，并关闭页面
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        TextView idTx=view.findViewById(R.id.addressId);//id
        TextView nameTx=view.findViewById(R.id.tv_address_name);
        TextView phoneTx=view.findViewById(R.id.tv_address_phone);
        TextView detaiTx=view.findViewById(R.id.tv_address_detai);

        String id_data=idTx.getText().toString().trim();
        String name=nameTx.getText().toString().trim();
        String phone=phoneTx.getText().toString().trim();
        String detai=detaiTx.getText().toString().trim();

        //存入Address地址信息
        SharedPreferences.Editor editor=sp.edit();
        editor.putString("id",id_data);
        editor.putString("name",name);
        editor.putString("phone",phone);
        editor.putString("detai",detai);
        editor.apply();

        Log.e("OnClickAddressData", "----------------------------------------------->id="+id_data+name+phone+detai);
        finish();
    }
}