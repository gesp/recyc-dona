package com.jianye.recycdona.model.fragment.wlfare.commodityorder.orderfragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.ComOrder;
import com.jianye.recycdona.control.http.CommoditOrderHttp;
import com.jianye.recycdona.model.fragment.wlfare.adapter.ComOrderAdapter;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.FragCommodityOderActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;

/**
 * 交易完成
 */
public class TranFinishFragment extends Fragment implements AdapterView.OnItemClickListener {



    ListView tranfinish_listview;

    private   View view;
    private ArrayList<ComOrder.DataBean> tranfinish;
    private ComOrderAdapter commoditAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view==null){
            view=inflater.inflate( R.layout.fragment_tran_finish, container, false);
        }

        initData();   //加载数据，初始化数据，初始化对象

        return view;
    }

    public void initData() {

        tranfinish_listview =view.findViewById(R.id.tranfinish_listview);
        tranfinish =new ArrayList<>();
        //适配数据
        //过滤
        for (ComOrder.DataBean c: FragCommodityOderActivity.list) {
            if (c.getStatus().equals("交易完成,用户已收货")||c.getStatus().equals("交易完成,用户未收货")){
                tranfinish.add(c);
            }
            Log.d(TAG, "loadingDatas: "+c.getStatus());
        }

        //倒序
//        Collections.reverse(tranfinish);
        //设置适配器
        commoditAdapter=new ComOrderAdapter(tranfinish,getContext());
        tranfinish_listview.setAdapter(commoditAdapter);
        tranfinish_listview.setOnItemClickListener(this);

        //ListView item按钮的点击事件
     commoditAdapter.setOnItemDeleteClickListener(new ComOrderAdapter.onItemDeleteListener() {
         @Override
         public void onDeleteClick(int i) {
             if (tranfinish.size()>0){
                 tranfinish.clear();
             }
             //重新获取数据
             List<ComOrder.DataBean > list = null;
             try {
                 list= CommoditOrderHttp.getMyOkHttp().getAllComOrder(Integer.parseInt(FragCommodityOderActivity.parentId));
             } catch (ExecutionException | InterruptedException e) {
                 e.printStackTrace();
             }

             //数据过滤
             assert list != null;
             for (ComOrder.DataBean c: list) {
                 if (c.getStatus().equals("交易完成,用户已收货")||c.getStatus().equals("交易完成,用户未收货")){
                     tranfinish.add(c);
                 }
                 Log.d(TAG, "loadingDatas: "+c.getStatus());
             }

             //倒序
//            Collections.reverse(tranfinish);

             commoditAdapter.notifyDataSetChanged();
         }
     });

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Log.d(TAG, "onItemClick: --------->position"+position);

    }



}

