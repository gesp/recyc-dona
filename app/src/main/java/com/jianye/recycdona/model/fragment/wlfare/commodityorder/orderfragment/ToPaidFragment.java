package com.jianye.recycdona.model.fragment.wlfare.commodityorder.orderfragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.ComOrder;
import com.jianye.recycdona.model.fragment.wlfare.adapter.ComOrderAdapter;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.FragCommodityOderActivity;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * 待支付
 */
public class ToPaidFragment extends Fragment implements AdapterView.OnItemClickListener {


    ListView topaid_listview;

    private   View view;
    private ArrayList<ComOrder.DataBean> topaid;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view==null){
            view=inflater.inflate( R.layout.fragment_to_paid, container, false);
        }

        initData();   //加载数据，初始化数据，初始化对象

        return view;
    }

    public void initData() {

        topaid_listview =view.findViewById(R.id.topaid_listview);
        topaid =new ArrayList<>();

        //过滤
        for (ComOrder.DataBean c: FragCommodityOderActivity.list) {
            if (c.getStatus().equals("待支付")){
                topaid.add(c);
            }
            Log.d(TAG, "loadingDatas: "+c.getStatus());
        }

        //倒序
//        Collections.reverse(topaid);


        //设置适配器
        ComOrderAdapter commoditAdapter=new ComOrderAdapter(topaid,getContext());
        topaid_listview.setAdapter(commoditAdapter);

        topaid_listview.setOnItemClickListener(this);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Log.d(TAG, "onItemClick: --------->position"+position);

    }

}