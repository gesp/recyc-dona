package com.jianye.recycdona.model.fragment.find;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jianye.recycdona.R;
import com.jianye.recycdona.model.fragment.FindFragment;
import com.jianye.recycdona.model.fragment.find.adapter.FindAllListAdapter;
import com.jianye.recycdona.model.fragment.find.bean.Find;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
//环保

public class EnvironFragment extends Fragment {

    View view;

    //id
    RecyclerView find_Environ_list;
    FindAllListAdapter findAllListAdapter;
    List<Find.ContentBean> listfindAll;

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view==null){
            view=inflater.inflate(R.layout.fragment_environ, container, false);
        }

        initFindById();

        return view;
    }

    public void initFindById() {
        List<Find.ContentBean> find_EnvironList=new ArrayList<>();

        find_Environ_list =view.findViewById(R.id.find_Environ_list);
        listfindAll=new ArrayList<>();

        listfindAll= FindFragment.Findlist;

        for (Find.ContentBean c :listfindAll) {
            Log.e(TAG, "initFindById: "+c);
            if (c.getLabel().equals("环保")){
                find_EnvironList.add(c);
            }
        }
        Log.e(TAG, "initFindById: "+listfindAll.size());

        findAllListAdapter=new FindAllListAdapter(R.layout.find_layout_list,find_EnvironList);
        findAllListAdapter.setContext(getContext());

        //布局管理器，
        LinearLayoutManager layoutManager1=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        find_Environ_list.setLayoutManager(layoutManager1);

        find_Environ_list.setNestedScrollingEnabled(false);
        find_Environ_list.setAdapter(findAllListAdapter);
    }
}