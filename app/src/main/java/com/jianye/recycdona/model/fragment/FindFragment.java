package com.jianye.recycdona.model.fragment;


import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.Carousel;
import com.jianye.recycdona.control.http.CommoditOrderHttp;
import com.jianye.recycdona.control.http.OrderHttp;
import com.jianye.recycdona.model.arryutils.ScreenUtil;
import com.jianye.recycdona.model.fragment.find.ActivityFragment;
import com.jianye.recycdona.model.fragment.find.AllfindFragment;
import com.jianye.recycdona.model.fragment.find.EnvironFragment;
import com.jianye.recycdona.model.fragment.find.MediaFragment;
import com.jianye.recycdona.model.fragment.find.WelFragment;
import com.jianye.recycdona.model.fragment.find.bean.Find;
import com.jianye.recycdona.model.fragment.wlfare.ViewPagerForScrollView;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.stx.xhb.xbanner.XBanner;
import com.stx.xhb.xbanner.transformers.Transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class FindFragment extends Fragment  {

    private View view;
    private XBanner mBanner;

    TextView mDic_all;
    TextView mDicOne;
    TextView mDicTwo;
    TextView mDicThree;
    TextView mDicFoun;
    //下拉刷新控件
     RefreshLayout refreshLayout;

    public static List<Find.ContentBean>  Findlist;

    ViewPagerForScrollView viewPager;


    private final AllfindFragment allfindFragment=new AllfindFragment();//所有
    private final WelFragment welfragment = new WelFragment();//公益
    private final EnvironFragment environFragment = new EnvironFragment();//环保
    private final ActivityFragment activityFragment = new ActivityFragment();//活动
    private final MediaFragment mediaFragment=new MediaFragment();//媒体

    private FindAdapter mAdapter;
    private Unbinder unbinder;
    private List<Carousel.ContentBean> images;
    private ArrayList<String> titles;

    public static FindFragment newInstance(String info) {
        Bundle args = new Bundle();
        FindFragment fragment = new FindFragment();
        args.putString("info", info);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Fresco.initialize(getActivity());
        getActivity().getWindow().setTitle(getArguments().getString("info"));

        if (view==null){
            view = inflater.inflate(R.layout.fragmet_find, container, false);
        }
        initViews(view);
        return view;

    }
    //初始化id
    private void initViews(View view) {

        refreshLayout=view.findViewById(R.id.find_refreshLayout);
        mBanner =view.findViewById(R.id.banner);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ScreenUtil.getScreenWidth(getActivity()) / 2);
        mBanner.setLayoutParams(layoutParams);
        mBanner.setPageTransformer(Transformer.Default);

        ViewPagetInit();    //viewPager滑动事件监听

        initBanner(mBanner);//Banner点击事件
        initData();//初始化轮播图数据

        //下拉刷新
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {

                Toast.makeText(getContext(),"我是有底线的",Toast.LENGTH_LONG).show();
                refreshLayout.finishLoadMore();//结束加载
            }
        });
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                initData();
                int item=viewPager.getCurrentItem();


                if (item==0){
                    allfindFragment.initFindById();
                }

                if (item==1){
                    welfragment.initFindById();
                }

                if (item==2){
                    environFragment.initFindById();
                }
                if (item==3){
                    activityFragment.initFindById();
                }
                if (item==4){
                    mediaFragment.initFindById();
                }
                Toast.makeText(getContext(),"刷新成功",Toast.LENGTH_LONG).show();
                refreshLayout.finishRefresh();//结束刷新
            }
        });

    }

    /**
     * findfragment页面初始化
     * viewPager滑动事件监听
     */
    private void ViewPagetInit() {

        mDic_all=view.findViewById(R.id.dic_all);

        mDicOne=view.findViewById(R.id.dic_one);

        mDicTwo=view.findViewById(R.id.dic_two);

        mDicThree=view.findViewById(R.id.dic_three);

        mDicFoun=view.findViewById(R.id.dic_foun);

        viewPager=view.findViewById(R.id.home_main);

        ButterKnife.bind(this, view);
        mAdapter = new FindAdapter(getChildFragmentManager());
        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(mAdapter);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                resetBackground();
                switch (position)
                {
                    case 0:
                        mDic_all.setBackgroundResource(R.color.blue);
                        break;
                    case 1:
                        mDicOne.setBackgroundResource(R.color.blue);
                        break;
                    case 2:
                        mDicTwo.setBackgroundResource(R.color.blue);
                        break;
                    case 3:
                        mDicThree.setBackgroundResource(R.color.blue);
                        break;
                    case  4:
                        mDicFoun.setBackgroundResource(R.color.blue);
                        break;
                    default:
                        break;
                }
            }

            @SuppressLint({"NonConstantResourceId", "ResourceAsColor"})
            @Override
            public void onPageScrollStateChanged(int state) {

                switch (view.getId())
                {
                    case R.id.find_home_all:
                        mDic_all.setBackgroundResource(R.color.blue);
                        break;
                    case R.id.find_home_one:
                        mDicOne.setBackgroundResource(R.color.blue);
                        break;

                    case R.id.find_home_two:
                        mDicTwo.setBackgroundResource(R.color.blue);
                        break;

                    case R.id.find_home_three:
                        mDicThree.setBackgroundResource(R.color.blue);
                        break;

                    case R.id.find_home_foun:
                        mDicFoun.setBackgroundResource(R.color.blue);
                        break;

                    default:
                        break;
                }
            }
        });
    }



    //引导页面设置广告图片点击事件
    private void initBanner(XBanner banner) {

        banner.setOnItemClickListener(new XBanner.OnItemClickListener() {
            @Override
            public void onItemClick(XBanner banner, Object model, View view, int position) {
                Toast.makeText(getActivity(), "点击了第" + (position + 1) + "图片", Toast.LENGTH_SHORT).show();
            }
        });

        //加载广告图片
        banner.loadImage(new XBanner.XBannerAdapter() {
            @Override
            public void loadBanner(XBanner banner, Object model, View view, int position) {
                //此处适用Fresco加载图片，可自行替换自己的图片加载框架
                SimpleDraweeView draweeView = (SimpleDraweeView) view;
                Carousel.ContentBean listBean = (( Carousel.ContentBean) model);
                String url = listBean.getCarousel();
                draweeView.setImageURI(Uri.parse(url));

            }
        });
    }



    /**
     * 初始化轮播数据和----->页面数据
     */
    private void initData()  {

        images = new ArrayList<>();
//        titles = new ArrayList<>();

        try {
            images= OrderHttp.getMyOkHttp().getCarousel();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        mBanner.setAutoPlayAble(images.size() > 1);
        mBanner.setIsClipChildrenMode(true);
        mBanner.setBannerData(R.layout.layout_fresco_imageview, images);
//        titles.add("这是第1张图片");
//        titles.add("第2张图片");
//        titles.add("这是第3张图片");
//        titles.add("这是第4张图片");


        //发起请求----->页面数据
        Findlist=new ArrayList<>();
        if (Findlist.size()<=0){
            try {
                Findlist=CommoditOrderHttp.getMyOkHttp().getrecyclerFind();

            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    //重置指示器颜色
    private void resetBackground()
    {    mDic_all.setBackgroundResource(R.color.white);
        mDicOne.setBackgroundResource(R.color.white);
        mDicThree.setBackgroundResource(R.color.white);
        mDicTwo.setBackgroundResource(R.color.white);
        mDicFoun.setBackgroundResource(R.color.white);
    }

    /**
     * 滑动点击事件监听器
     * @param
     */
    @SuppressLint({"ResourceAsColor", "NonConstantResourceId"})
    @OnClick({R.id.find_home_all,R.id.find_home_one, R.id.find_home_two, R.id.find_home_three,R.id.find_home_foun})
    public void onViewClicked(View view)
    {
        resetBackground();
        switch (view.getId())
        {

            case R.id.find_home_all:
                mDic_all.setBackgroundResource(R.color.blue);
                viewPager.setCurrentItem(0);
                break;
            case R.id.find_home_one:
                mDicOne.setBackgroundResource(R.color.blue);
//                mDicOne.setTextColor(R.color.black1);
                viewPager.setCurrentItem(1);
                break;
            case R.id.find_home_two:
                mDicTwo.setBackgroundResource(R.color.blue);
//                mDicTwo.setTextColor(R.color.black1);
                viewPager.setCurrentItem(2);
                break;
            case R.id.find_home_three:
                mDicThree.setBackgroundResource(R.color.blue);
//                mDicThree.setTextColor(R.color.black1);
                viewPager.setCurrentItem(3);
                break;
            case R.id.find_home_foun:
                mDicFoun.setBackgroundResource(R.color.blue);
//                mDicFoun.setTextColor(R.color.black1);
                viewPager.setCurrentItem(4);
                break;
            default:
                break;
        }
    }


    /**
     * Fragment适配器
     */
    private class FindAdapter extends FragmentPagerAdapter {
        private List<Fragment> mList;
        public FindAdapter(@NonNull FragmentManager fm) {
            super(fm);
            mList = new ArrayList<>();

            mList.add(allfindFragment);
            mList.add(welfragment);
            mList.add(environFragment);
            mList.add(activityFragment);
            mList.add(mediaFragment);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mList.get(position);
        }

        @Override
        public int getCount() {
            return mList == null?0:mList.size();
        }
    }
}
