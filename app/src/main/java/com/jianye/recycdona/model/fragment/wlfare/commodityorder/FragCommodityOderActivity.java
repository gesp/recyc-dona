package com.jianye.recycdona.model.fragment.wlfare.commodityorder;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.ComOrder;
import com.jianye.recycdona.control.http.CommoditOrderHttp;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.orderfragment.ComOrderAllFragment;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.orderfragment.ShopFinishFragment;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.orderfragment.ToPaidFragment;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.orderfragment.ToShipFragment;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.orderfragment.TranFinishFragment;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.zhy.autolayout.AutoRelativeLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 商品订单
 */
public class FragCommodityOderActivity extends AppCompatActivity implements View.OnClickListener {

    private final ComOrderAllFragment comOrderAllFragment=new ComOrderAllFragment();//全部
    private final ShopFinishFragment shopFinishFragment=new ShopFinishFragment();//已发货
    private final ToShipFragment toShipFragment=new ToShipFragment();//待发货
    private final ToPaidFragment toPaidFragment=new ToPaidFragment();//待支付
    private final TranFinishFragment tranFinishFragment=new TranFinishFragment();//交易完成
    private CommdityAdapter mAdapter;
    private ViewPager viewPager;
    private TextView mDic_all;
    private TextView mDicOne,mDicTwo,mDicThree,mDicFoun;
    private List<Fragment> mList;
    private AutoRelativeLayout commodit_all,commodit_home_one,commodit_home_two,commodit_home_three,commodit_home_foun;
    private RefreshLayout refreshLayout;
    SharedPreferences configsp;
    public static List<ComOrder.DataBean> list;
    public static String parentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commdity_oder);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//竖屏
        ActionBar actionBar=getSupportActionBar();

        if (actionBar!=null){
            actionBar.hide();
        }
        initFindviewById();
        ViewPagetInit();
    }

    private void initFindviewById() {

        refreshLayout=findViewById(R.id.refreshLayout);
        configsp=getSharedPreferences("config", MODE_PRIVATE);//confi 文件名
        parentId=configsp.getString("id","");
        try {
            mList=new ArrayList<>();
            list= CommoditOrderHttp.getMyOkHttp().getAllComOrder(Integer.parseInt(parentId));
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        viewPager=findViewById(R.id.com_home_main);

        commodit_all=findViewById(R.id.commodit_all);
        commodit_all.setOnClickListener(this);
        commodit_home_one=findViewById(R.id.commodit_home_one);
        commodit_home_one.setOnClickListener(this);
        commodit_home_two=findViewById(R.id.commodit_home_two);
        commodit_home_two.setOnClickListener(this);
        commodit_home_three=findViewById(R.id.commodit_home_three);
        commodit_home_three.setOnClickListener(this);
        commodit_home_foun=findViewById(R.id.commodit_home_foun);
        commodit_home_foun.setOnClickListener(this);

    }

    private void ViewPagetInit() {

        mDic_all=findViewById(R.id.com_dic_all);
        mDicOne=findViewById(R.id.com_dic_one);
        mDicTwo=findViewById(R.id.com_dic_two);
        mDicThree=findViewById(R.id.com_dic_three);
        mDicFoun=findViewById(R.id.com_dic_foun);
        viewPager=findViewById(R.id.com_home_main);

        ButterKnife.bind(this, FragCommodityOderActivity.this);
        mAdapter = new CommdityAdapter(getSupportFragmentManager());
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(mAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                resetBackground();
                switch (position)
                {
                    case 0:
                        mDic_all.setBackgroundResource(R.color.blue);
                        break;
                    case 1:
                        mDicOne.setBackgroundResource(R.color.blue);
                        break;
                    case 2:
                        mDicTwo.setBackgroundResource(R.color.blue);
                        break;
                    case 3:
                        mDicThree.setBackgroundResource(R.color.blue);
                        break;
                    case  4:
                        mDicFoun.setBackgroundResource(R.color.blue);
                        break;
                    default:
                        break;
                }
            }

            @SuppressLint({"NonConstantResourceId", "ResourceAsColor"})
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



        //下拉刷新
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                int viewpage=viewPager.getCurrentItem();//获取当前fragment位置

                //防止数据重复
                if (list!=null)list.clear();

                assert parentId!=null;
                try {
                    list= CommoditOrderHttp.getMyOkHttp().getAllComOrder(Integer.parseInt(parentId));
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
                Log.e("CurrentIte", "onRefresh: "+viewpage);


                if (viewpage==0){
                    comOrderAllFragment.initData();
                }
                if (viewpage==1){
                    toPaidFragment.initData();
                }
                if (viewpage==2){
                    toShipFragment.initData();
                }
                if (viewpage==3){
                    shopFinishFragment.initData();
                }
                if (viewpage==4){
                    tranFinishFragment.initData();

                }



             Toast.makeText(FragCommodityOderActivity.this,"刷新成功",Toast.LENGTH_LONG).show();
                refreshLayout.finishRefresh();//结束刷新
            }
        });






    }



    //重置指示器颜色
    private void resetBackground()
    {
        mDic_all.setBackgroundResource(R.color.white);
        mDicOne.setBackgroundResource(R.color.white);
        mDicThree.setBackgroundResource(R.color.white);
        mDicTwo.setBackgroundResource(R.color.white);
        mDicFoun.setBackgroundResource(R.color.white);
    }

    /**
     * 滑动点击事件监听器
     * @param
     */
    @SuppressLint({"ResourceAsColor", "NonConstantResourceId"})
    @OnClick({R.id.commodit_all,R.id.commodit_home_one, R.id.commodit_home_two, R.id.commodit_home_three,R.id.commodit_home_foun})
    public void onViewClicked(View view)
    {
        resetBackground();
        switch (view.getId())
        {
            case R.id.commodit_all:
                mDic_all.setBackgroundResource(R.color.blue);
                viewPager.setCurrentItem(0);
                break;
            case R.id.commodit_home_one:
                mDicOne.setBackgroundResource(R.color.blue);
//                mDicOne.setTextColor(R.color.black1);
                viewPager.setCurrentItem(1);
                break;
            case R.id.commodit_home_two:
                mDicTwo.setBackgroundResource(R.color.blue);
//                mDicTwo.setTextColor(R.color.black1);
                viewPager.setCurrentItem(2);
                break;
            case R.id.commodit_home_three:
                mDicThree.setBackgroundResource(R.color.blue);
//                mDicThree.setTextColor(R.color.black1);
                viewPager.setCurrentItem(3);
                break;
            case R.id.commodit_home_foun:
                mDicFoun.setBackgroundResource(R.color.blue);
//                mDicFoun.setTextColor(R.color.black1);
                viewPager.setCurrentItem(4);
                break;
            default:
                break;
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        resetBackground();
        switch (view.getId())
        {
            case R.id.commodit_all:
                mDic_all.setBackgroundResource(R.color.blue);
                viewPager.setCurrentItem(0);
                break;
            case R.id.commodit_home_one:
                mDicOne.setBackgroundResource(R.color.blue);
                viewPager.setCurrentItem(1);
                break;

            case R.id.commodit_home_two:
                mDicTwo.setBackgroundResource(R.color.blue);
                viewPager.setCurrentItem(2);
                break;

            case R.id.commodit_home_three:
                mDicThree.setBackgroundResource(R.color.blue);
                viewPager.setCurrentItem(3);
                break;

            case R.id.commodit_home_foun:
                mDicFoun.setBackgroundResource(R.color.blue);
                viewPager.setCurrentItem(4);
                break;

            default:
                break;
        }
    }

    public void Finish(View view) {
        finish();
    }

    private class CommdityAdapter extends FragmentPagerAdapter {

        public CommdityAdapter(@NonNull FragmentManager fm) {
            super(fm);
            mList = new ArrayList<>();
            mList.add(comOrderAllFragment);
            mList.add(toPaidFragment);
            mList.add(toShipFragment);
            mList.add(shopFinishFragment);
            mList.add(tranFinishFragment);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mList.get(position);
        }

        @Override
        public int getCount() {
            return mList == null?0:mList.size();
        }
    }
}