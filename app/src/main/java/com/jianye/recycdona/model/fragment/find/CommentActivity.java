package com.jianye.recycdona.model.fragment.find;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.jianye.recycdona.R;
import com.jianye.recycdona.control.http.CommentHttp;
import com.jianye.recycdona.model.fragment.find.adapter.CommentAdapter;
import com.jianye.recycdona.model.fragment.find.bean.Comment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;

public class CommentActivity extends AppCompatActivity implements View.OnClickListener {


    TextView cname,cdecl,clable,ccontent,cfind_data,ccomment_number;
    ImageView icon,find_image1,find_image2,find_image3,find_image4,find_image5,find_image6,one_img, Commentcomtent;
    GridLayout gridLayout;
    private RecyclerView recyclerView;
    String findId,parentId;
    private SharedPreferences confg_sp;
    private Button btn_postcommentData;
    private EditText comment_content_edditext;
    private String username;
    private String content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ActionBar actionBar=getSupportActionBar();

        if (actionBar!=null){
            actionBar.hide();
        }
        initFindView();
        initData();

        http();



    }

    private void http() {

        if (parentId!=null){
            try {
                List<Comment.DataBean> dataBeans= CommentHttp.getMyOkHttp().getCommentInfo(findId);
                for (Comment.DataBean d :dataBeans) {
                    Log.d(TAG, "CommentActivity:----------------------------->"+d.getComentinfo());
                }
                LinearLayoutManager layoutManager1=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
                recyclerView.setLayoutManager(layoutManager1);
                recyclerView.setNestedScrollingEnabled(false);
                CommentAdapter adapter=new CommentAdapter(R.layout.comment_layout_recyclerview,dataBeans);
                recyclerView.setAdapter(adapter);

            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }


    }

    private void initData() {
        Intent intent=getIntent();
        String name=intent.getStringExtra("name");
        String decl=intent.getStringExtra("decl");
        String lable=intent.getStringExtra("lable");
         content=intent.getStringExtra("content");
        String images=intent.getStringExtra("images");
        String find_data=intent.getStringExtra("find_data");
        String commentnumber=intent.getStringExtra("commentnumber");
        findId=intent.getStringExtra("id");
        confg_sp=getSharedPreferences("config", MODE_PRIVATE);//confi 用户数据文件
        parentId=confg_sp.getString("id","");
        username=confg_sp.getString("username","");



        icon.setImageResource(R.drawable.logo);
        cname.setText(name);
        cdecl.setText(decl);
        clable.setText(lable);
        ccontent.setText(content);
        cfind_data.setText(find_data);
        ccomment_number.setText(commentnumber);
        ImageView[] imageViews=new ImageView[]{find_image1,find_image2
                ,find_image3,find_image4,find_image5,find_image6};
        assert images != null;
        String[] arry=images.split(",");
        int imglength=arry.length;
        if (imglength>1){
            for (int i=0;i<imglength;i++){
                Glide.with(this).load(arry[i]).into(imageViews[i]);
                imageViews[i].setVisibility(View.VISIBLE);
            }
        }
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (imglength==1){
            Glide.with(this).load(arry[0]).into(one_img);
            one_img.setVisibility(View.VISIBLE);
        }

        ccomment_number.setText(commentnumber);

    }


    private void initFindView() {


        cname = findViewById(R.id.tv_find_name);
         cdecl = findViewById(R.id.tv_xuanyan);
        clable = findViewById(R.id.tv_label);
        icon=findViewById(R.id.img_heade);
        ccontent = findViewById(R.id.tv_content);
        gridLayout=findViewById(R.id.gridlayout_image);

        find_image1=findViewById(R.id.find_image1);
        find_image2=findViewById(R.id.find_image2);
        find_image3=findViewById(R.id.find_image3);
        find_image4=findViewById(R.id.find_image4);
        find_image5=findViewById(R.id.find_image5);
        find_image6=findViewById(R.id.find_image6);
        one_img=findViewById(R.id.one_img);
        Commentcomtent=findViewById(R.id.image_comment);
        cfind_data=findViewById(R.id.find_data);//时间
        ccomment_number=findViewById(R.id.comment_number);//评论数
        recyclerView =findViewById(R.id.commentList);
        btn_postcommentData=findViewById(R.id.btn_postcommentData);//提交评论
        btn_postcommentData.setOnClickListener(this);
        comment_content_edditext=findViewById(R.id.comment_content_edditext);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_postcommentData:
                String commentinfo=comment_content_edditext.getText().toString().trim();
                if (commentinfo==null){
                    Toast.makeText(CommentActivity.this,"评论内容不能为空！",Toast.LENGTH_LONG).show();
                }else if (commentinfo.length()>=300){
                    Toast.makeText(CommentActivity.this,"评论内容不超过300字！",Toast.LENGTH_LONG).show();

                }else {
//                    String findidint, String parenidint, String namevarchar, String cmtcontentvarchar,
//                            String thumbsnumint, String cretedatadatetime, String comentinfovarchar
                    String data=getTime(new Date());
                    try {
                       String dataBeans= CommentHttp.getMyOkHttp().postCommentInfo(findId,parentId,username,
                                content,"0",data,commentinfo);
                        Log.d(TAG, "commentPsot---onClick: "+dataBeans);
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }
                    // 隐藏软键盘
                    InputMethodManager imm = (InputMethodManager) CommentActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(
                         CommentActivity.this.getWindow().getDecorView().getWindowToken(), 0);
                    }

                    http();
                    comment_content_edditext.setText("");
                    String num=ccomment_number.getText().toString().trim();
                    int commentNunber=Integer.parseInt(num)+1;
                    ccomment_number.setText(commentNunber+"");


                }
        }

    }

    //获取当前时间
    public String getTime(Date date) {//可根据需要自行截取数据显示
        Log.e("getTime()", "choice date millis: " + date.getTime());
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }

    public void Finish(View view) {
        finish();
    }
}