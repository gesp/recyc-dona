package com.jianye.recycdona.model.fragment.wlfare.commodityorder.bean;

import com.stx.xhb.xbanner.entity.SimpleBannerInfo;

public class commodity  extends SimpleBannerInfo {
    private String carousel;

    public commodity(String carousel) {
        this.carousel = carousel;
    }

    @Override
    public Object getXBannerUrl() {
        return getCarousel();
    }

    public String getCarousel() {
        return carousel;
    }

    public void setCarousel(String carousel) {
        this.carousel = carousel;
    }
}
