package com.jianye.recycdona.model.fragment.wlfare.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.Credits;
import com.jianye.recycdona.model.fragment.home.RecyclerpActivity;

import java.util.List;

public class CreditsAdapter extends BaseAdapter {

    List<Credits.ContentBean> list;
    Context context;

    public CreditsAdapter(List<Credits.ContentBean> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    CreditsViewHolder viewHolder;
    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){

            viewHolder = new  CreditsViewHolder();

            convertView= LayoutInflater.from(context).inflate(R.layout.credits_layout_list, parent,false);
            viewHolder.image=convertView.findViewById(R.id.credits_image);
            viewHolder.name=convertView.findViewById(R.id.credits_name);
            viewHolder.point=convertView.findViewById(R.id.credits_point);
            viewHolder.info=convertView.findViewById(R.id.credits_info);
            viewHolder.btn=convertView.findViewById(R.id.credits_btn);
            convertView.setTag(viewHolder);//讲ViewHolder存储在View中

        }else{
            viewHolder = (CreditsViewHolder) convertView.getTag();//重获取viewHolder
        }

        Glide.with(context)
                .load(list.get(position).getPath())
                .into(viewHolder.image);
        viewHolder.name.setText(list.get(position).getTitle());
        viewHolder.point.setText("+"+list.get(position).getCreditsnum()+"");
        viewHolder.info.setText(list.get(position).getInfo());


        if (viewHolder.btn.getText().equals("去回收")){
            viewHolder.btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, RecyclerpActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(new Intent(intent));
                }
            });
        }



        return convertView;
    }

    static class CreditsViewHolder{

        TextView name,info,point,btn;
        ImageView image;
    }
}
