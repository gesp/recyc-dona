package com.jianye.recycdona.model.arryutils;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;



public class CacheImageView extends AppCompatImageView
{

    public CacheImageView(Context context)
    {
        super(context);
    }

    public CacheImageView(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
    }

    public CacheImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        setImageDrawable(null);
    }
}
