package com.jianye.recycdona.model.fragment.home.bean;

import java.util.List;


public class addressInfo {


    private List<DataBean> data;
    private String msg;
    private Integer status;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public static class DataBean {
        private String address;
        private String detaiAddress;
        private Integer id;
        private Integer parentId;
        private String phone;
        private String receiver;

        public DataBean(String address, String detaiAddress, Integer id, Integer parentId, String phone, String receiver) {
            this.address = address;
            this.detaiAddress = detaiAddress;
            this.id = id;
            this.parentId = parentId;
            this.phone = phone;
            this.receiver = receiver;
        }

        public DataBean() {
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDetaiAddress() {
            return detaiAddress;
        }

        public void setDetaiAddress(String detaiAddress) {
            this.detaiAddress = detaiAddress;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getParentId() {
            return parentId;
        }

        public void setParentId(Integer parentId) {
            this.parentId = parentId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getReceiver() {
            return receiver;
        }

        public void setReceiver(String receiver) {
            this.receiver = receiver;
        }
    }
}
