package com.jianye.recycdona.model.fragment.find;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jianye.recycdona.R;
import com.jianye.recycdona.model.fragment.FindFragment;
import com.jianye.recycdona.model.fragment.find.adapter.FindAllListAdapter;
import com.jianye.recycdona.model.fragment.find.bean.Find;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;


public class AllfindFragment extends Fragment {


    View view;

    //id
    RecyclerView findAllrecyclerView;
    FindAllListAdapter findAllListAdapter;
    List<Find.ContentBean> listfindAll;

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view==null){
            view=inflater.inflate(R.layout.fragment_allfind, container, false);
        }

        initFindById();

        return view;
    }

    public void initFindById() {
        findAllrecyclerView =view.findViewById(R.id.find_all_list);
        listfindAll=new ArrayList<>();

        listfindAll= FindFragment.Findlist;

        for (Find.ContentBean c :listfindAll) {
            Log.e(TAG, "initFindById: "+c);
        }
        Log.e(TAG, "initFindById: "+listfindAll.size());

        findAllListAdapter=new FindAllListAdapter(R.layout.find_layout_list,listfindAll);
        findAllListAdapter.setContext(getContext());

        //布局管理器，
        LinearLayoutManager layoutManager1=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        findAllrecyclerView.setLayoutManager(layoutManager1);

        findAllrecyclerView.setNestedScrollingEnabled(false);
        findAllrecyclerView.setAdapter(findAllListAdapter);
    }


}