package com.jianye.recycdona.model.fragment.mine;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.RecyclerOrder;
import com.jianye.recycdona.control.http.OrderHttp;
import com.jianye.recycdona.model.fragment.mine.reserveoderfragment.AllAppointmentsFragment;
import com.jianye.recycdona.model.fragment.mine.reserveoderfragment.CompleteOrderFragment;
import com.jianye.recycdona.model.fragment.mine.reserveoderfragment.InProgressFragment;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class FragReseverOrderActivity extends AppCompatActivity implements View.OnClickListener {


    private ViewPager mViewPager;
    private TextView tv_order_all,tv_order_jingxing,tv_order_finish;
    private TextView tv_order_all2,tv_order_jingxing2,tv_order_finish2;
    private OderAdapterFragment mAdapter;


    private final AllAppointmentsFragment allAppointmentsFragment=new AllAppointmentsFragment();//所有预约
    private final CompleteOrderFragment completeOrderFragment = new CompleteOrderFragment();//完成订单
    private final InProgressFragment inProgressFragment = new InProgressFragment();//正在进行
    private RefreshLayout refreshLayout;
    private SharedPreferences confg_sp;

    List<RecyclerOrder.DataBean> delist=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//竖屏
        ActionBar actionBar=getSupportActionBar();
        if (actionBar!=null){
            actionBar.hide();
        }

        ViewPagetInit();//初始化
        //点击过来的页面显示
        Intent inn=getIntent();
        int page=inn.getIntExtra("page",0);
        Log.i("Page","----------------------->"+page+"");

        mViewPager.setCurrentItem(page-1);


    }


//结束
    public void Finish(View view) {
        finish();
    }


    /***
     * Fragmen适配器
     */
    public class OderAdapterFragment extends FragmentPagerAdapter {

        private final ArrayList<Fragment> mList;

        public OderAdapterFragment(@NonNull FragmentManager fm) {
            super(fm);

            mList = new ArrayList<>();//注意顺序
            mList.add(allAppointmentsFragment);
            mList.add(inProgressFragment);
            mList.add(completeOrderFragment);

        }


        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mList.get(position);
        }

        @Override
        public int getCount() {
            return mList == null?0:mList.size();
        }
    }


    //初始化ID
        private void ViewPagetInit() {

        refreshLayout=findViewById(R.id.refreshLayout_order);
        tv_order_all=findViewById(R.id.tv_order_all);
        tv_order_all.setOnClickListener(this);
        tv_order_jingxing=findViewById(R.id.tv_order_jingxing);
        tv_order_jingxing.setOnClickListener(this);
        tv_order_finish=findViewById(R.id.tv_order_finish);
        tv_order_finish.setOnClickListener(this);

        tv_order_all2=findViewById(R.id.tv_order_all2);

        tv_order_jingxing2=findViewById(R.id.tv_order_jingxing2);
        tv_order_finish2=findViewById(R.id.tv_order_finish2);
        mViewPager=findViewById(R.id.order_viewPage);

//        ButterKnife.bind(this,);
        mAdapter = new OderAdapterFragment(getSupportFragmentManager());
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(mAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            resetBackground();
            switch (position)
            {
                case 0:
                    tv_order_all2.setBackgroundResource(R.color.line);
                    break;
                case 1:
                    tv_order_jingxing2.setBackgroundResource(R.color.line);
                    break;
                case 2:
                    tv_order_finish2.setBackgroundResource(R.color.line);
                    break;
                default:
                    break;
            }
        }

            @SuppressLint({"NonConstantResourceId", "ResourceAsColor"})
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



        //刷新
//        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
//
//                Toast.makeText(OrderActivity.this,"已经到底了",Toast.LENGTH_LONG).show();
//                refreshLayout.finishLoadMore();//结束加载
//            }
//        });

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {

                int viewpage=mViewPager.getCurrentItem();//获取当前fragment位置

                String id=getId();//获取用户id
                Log.e("CurrentIte", "onRefresh: "+viewpage);

                if (delist!=null)delist.clear();//防止数据重复

                try {
                    delist= OrderHttp.getMyOkHttp().getOrderInfo(id);//发起请求
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }


                if (viewpage==0){//第1个fragmen
                    allAppointmentsFragment.initFindViewId(1,delist);
                }

                if (viewpage==1){//第2个fragmen
                    inProgressFragment.initFindViewId(2,delist);
                }

                if (viewpage==2){//第3个fragmen
                    completeOrderFragment.initFindViewId(3,delist);
                }


                Toast.makeText(FragReseverOrderActivity.this,"刷新成功",Toast.LENGTH_LONG).show();
                refreshLayout.finishRefresh();//结束刷新
            }
        });

    }


    //重置指示器颜色
    private void resetBackground()
    {
        tv_order_all2.setBackgroundResource(R.color.white);
        tv_order_finish2.setBackgroundResource(R.color.white);
        tv_order_jingxing2.setBackgroundResource(R.color.white);
    }


    public String getId(){

        confg_sp=getSharedPreferences("config", MODE_PRIVATE);//confi 用户数据文件
        return confg_sp.getString("id","");
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        resetBackground();
        switch (view.getId())
        {

            case R.id.tv_order_all:
                tv_order_all2.setBackgroundResource(R.color.line);
                mViewPager.setCurrentItem(0);
                break;

            case R.id.tv_order_jingxing:
                tv_order_jingxing2.setBackgroundResource(R.color.line);
                mViewPager.setCurrentItem(1);
                break;

            case R.id.tv_order_finish:
                tv_order_finish2.setBackgroundResource(R.color.line);
                mViewPager.setCurrentItem(2);
                break;

            default:
                break;
        }
    }





    }