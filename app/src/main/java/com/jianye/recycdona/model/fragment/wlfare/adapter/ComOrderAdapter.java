package com.jianye.recycdona.model.fragment.wlfare.adapter;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.ComOrder;
import com.jianye.recycdona.control.http.CommoditOrderHttp;
import com.jianye.recycdona.control.http.UserOkHttp;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.ComOrderDetailsTowActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;


/**
 * 商品订单适配
 */
public class ComOrderAdapter extends BaseAdapter {

    List<ComOrder.DataBean> list=new ArrayList<>();

    Context context;
    private ComordeViewHolder viewHolder;
    View.OnClickListener listener;



    public ComOrderAdapter(List<ComOrder.DataBean> list, Context cntext) {
        this.list = list;
        this.context = cntext;

        this.listener=listener;
    }



    /**
     * 删除按钮的监听接口
     */
    public interface onItemDeleteListener {
        void onDeleteClick(int i);
    }

    private onItemDeleteListener mOnItemDeleteListener;

    public void setOnItemDeleteClickListener(onItemDeleteListener mOnItemDeleteListener) {
        this.mOnItemDeleteListener = mOnItemDeleteListener;
    }




    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView= LayoutInflater.from(context).inflate(R.layout.comorder_layout_recycler, parent,false);
            viewHolder = new ComordeViewHolder(convertView);
            convertView.setTag(viewHolder);//讲ViewHolder存储在View中

        }else{
            viewHolder = (ComordeViewHolder) convertView.getTag();//重获取viewHolder
        }

        viewHolder.confirmreceipt.setOnClickListener(listener);
        viewHolder.confirmreceipt.setTag(position);

        String[] status=list.get(position).getStatus().split(",");

        String[] orderinfpArry=list.get(position).getOrderinfo().split("\n");

        Log.d(TAG, "orderinfpArry: "+orderinfpArry.length);

        Log.d(TAG, "convert: "+orderinfpArry[0]+"\n"+orderinfpArry[1]+"\n"+orderinfpArry[2]);

        Glide.with(context).load(orderinfpArry[2]).into( viewHolder.commodit_imamge2);

        viewHolder.commdityOrder_name.setText(orderinfpArry[0]);
        viewHolder.commdityOrder_date.setText(list.get(position).getCreateDate());
        viewHolder.commodiyOrder_price.setText("¥ "+orderinfpArry[1]+"\t"+"("+list.get(position).getFreight()+")");
        viewHolder.comorder_status.setText(list.get(position).getStatus());

        if (list.get(position).getStatus().equals("交易完成,用户已收货")|| list.get(position).getStatus().equals("交易完成,用户未收货")){
            viewHolder.comorder_status.setText(status[0]);
        }


        //过滤待支付状态
        if (list.get(position).getStatus().equals("待支付")){

            //隐藏
            viewHolder.rightbtn.setVisibility(View.VISIBLE);
            viewHolder.centerbtn.setVisibility(View.GONE);
            viewHolder.confirmreceipt.setVisibility(View.GONE);
            //显示去支付
            viewHolder.leftbtn.setVisibility(View.VISIBLE);
            viewHolder.comorder_pai.setVisibility(View.VISIBLE);
        }


        if (list.get(position).getStatus().equals("订单取消")){

            viewHolder.centerbtn.setVisibility(View.VISIBLE);//显示删除
            //隐藏
            viewHolder.rightbtn.setVisibility(View.VISIBLE);
            viewHolder.confirmreceipt.setVisibility(View.GONE);
            viewHolder.comorder_pai.setVisibility(View.GONE);
            viewHolder.leftbtn.setVisibility(View.GONE);
        }

        if (list.get(position).getStatus().equals("待发货")) {
            viewHolder.rightbtn.setVisibility(View.VISIBLE);
            viewHolder.comorder_pai.setVisibility(View.GONE);
            viewHolder.centerbtn.setVisibility(View.GONE);
            viewHolder.confirmreceipt.setVisibility(View.GONE);
            //显示删除
            viewHolder.leftbtn.setVisibility(View.VISIBLE);


        }
        if (list.get(position).getStatus().equals("已发货")) {


            viewHolder.centerbtn.setVisibility(View.GONE);
            //隐藏
            viewHolder.rightbtn.setVisibility(View.VISIBLE);
            viewHolder.comorder_pai.setVisibility(View.GONE);
            viewHolder.leftbtn.setVisibility(View.GONE);
            viewHolder.confirmreceipt.setVisibility(View.GONE);
        }


        if (list.get(position).getStatus().equals("交易完成,用户未收货")) {

            viewHolder.rightbtn.setVisibility(View.VISIBLE);
            viewHolder.confirmreceipt.setVisibility(View.VISIBLE);
            //隐藏
            viewHolder.centerbtn.setVisibility(View.GONE);
            viewHolder.comorder_pai.setVisibility(View.GONE);
            viewHolder.leftbtn.setVisibility(View.GONE);
        }

        if (list.get(position).getStatus().equals("交易完成,用户已收货")) {
            viewHolder.rightbtn.setVisibility(View.VISIBLE);
            //隐藏
            viewHolder.centerbtn.setVisibility(View.GONE);
            viewHolder.confirmreceipt.setVisibility(View.GONE);
            viewHolder.comorder_pai.setVisibility(View.GONE);
            viewHolder.leftbtn.setVisibility(View.GONE);
        }

        //确认收货
        viewHolder.confirmreceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (list.get(position).getStatus().equals("交易完成,用户未收货")){

                    try {
                        CommoditOrderHttp.getMyOkHttp().putCommdityOrder(list.get(position).getId()+"","交易完成,用户已收货");
                        viewHolder.confirmreceipt.setVisibility(View.GONE);
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }

                    Log.d(TAG, "---------------------------->position"+position);
                    mOnItemDeleteListener.onDeleteClick(position);
                }
            }
        });

        //去支付点击事件
        viewHolder.comorder_pai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.comorder_pai.getText().toString().equals("去支付")){
                    //用户积分大于或等于实付积分
                    if (getPoint()>=list.get(position).getPaidpoint()){
                        try {
                            String code=CommoditOrderHttp.getMyOkHttp().putCommdityOrder(list.get(position).getId()+"","待发货");
                            if (code.equals("200")){
                                AloDialog("支付成功");

                            }else {
                                AloDialog("支付出错");
                            }
                        } catch (ExecutionException | InterruptedException e) {
                            e.printStackTrace();
                        }
                    }else {
                        AloDialog("积分不足，快去做任务吧");
                    }

                    //更新数据
//                    mOnItemDeleteListener.onDeleteClick(position);

                }
            }
        });


        //取消订单点击事件
        viewHolder.leftbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.leftbtn.getText().toString().trim().equals("取消订单")){
                    Dialog(position);
                }
            }
        });

        viewHolder.centerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.centerbtn.getText().toString().equals("删除订单")){
                    DeleteDialog( position);
                }

            }
        });


        //查看订单
        viewHolder.rightbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context, ComOrderDetailsTowActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                String[] status=list.get(position).getStatus().split(",");
                intent.putExtra("orderid",list.get(position).getId()+"");
                intent.putExtra("Status",status[0]);
                intent.putExtra("orderinfo",list.get(position).getOrderinfo());
                intent.putExtra("addressinfo",list.get(position).getAddressinfo());
                intent.putExtra("number",list.get(position).getNunber());
                intent.putExtra("paidpoint",list.get(position).getPaidpoint()+"");
                intent.putExtra("price",list.get(position).getPrice()+"");
                intent.putExtra("feight",list.get(position).getFreight());
                intent.putExtra("orderTime",list.get(position).getCreateDate());
                intent.putExtra("ordernum",list.get(position).getOrdernum());

                Log.d(TAG, "onClick: "+list.get(position).getPaidpoint()+"\n"+"paidpoint"+list.get(position).getPrice());
                context.startActivity(intent);

            }
        });

        return convertView;
    }



    static class ComordeViewHolder {
        TextView commodiyOrder_price,commdityOrder_name,commdityOrder_date,comorder_status,confirmreceipt;
        ImageView commodit_imamge2;
        Button leftbtn,rightbtn,comorder_pai,centerbtn;

        public ComordeViewHolder(View view) {
            commodiyOrder_price=view.findViewById(R.id.comorder_price);
            commdityOrder_name=view.findViewById(R.id.comorder_name);
            commdityOrder_date=view.findViewById(R.id.comorder_date);
            commodit_imamge2=view.findViewById(R.id.comorder_image);
            leftbtn =view.findViewById(R.id.comorder_cancel);
            comorder_status=view.findViewById(R.id.comorder_status);
            rightbtn=view.findViewById(R.id.tv_order_ReviseOder);
            centerbtn=view.findViewById(R.id.comorder_certer);
            //去支付
            comorder_pai=view.findViewById(R.id.comorder_pai);
            confirmreceipt=view.findViewById(R.id.confirmreceipt);


        }
    }

    public void Dialog(int position){

        AlertDialog.Builder normalMoreButtonDialog = new AlertDialog.Builder(context);
        normalMoreButtonDialog.setTitle("提示");
        normalMoreButtonDialog.setMessage("确定取消订单？");
        //设置按钮
        normalMoreButtonDialog.setPositiveButton("确定"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        Log.d(TAG, "onClick: -------------------------->确定取消");

                        try {
                            String code=CommoditOrderHttp.getMyOkHttp().putCommdityOrder(list.get(position).getId()+"","订单取消");
                            Log.d(TAG, "确定取消订单: "+code);
                        } catch (ExecutionException | InterruptedException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });

        normalMoreButtonDialog.setNeutralButton("取消"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });



        normalMoreButtonDialog.create().show();
    }

    public void DeleteDialog(int position){

        AlertDialog.Builder normalMoreButtonDialog = new AlertDialog.Builder(context);
        normalMoreButtonDialog.setTitle("提示");
        normalMoreButtonDialog.setMessage("删除后不可恢复，确定删除订单？");
        //设置按钮
        normalMoreButtonDialog.setPositiveButton("确定"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                            String code=CommoditOrderHttp.getMyOkHttp().deleteCommdityOrder(list.get(position).getId()+"");
                            Log.d(TAG, "onClick: "+code);
                             dialog.dismiss();
                    }
                });

        normalMoreButtonDialog.setNeutralButton("取消"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });



        normalMoreButtonDialog.create().show();
    }


    public  void AloDialog(String msg){
        AlertDialog.Builder normalMoreButtonDialog = new AlertDialog.Builder(context);
        normalMoreButtonDialog.setTitle("提示");
        normalMoreButtonDialog.setMessage(msg);
        //设置按钮
        normalMoreButtonDialog.setPositiveButton("确定"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        normalMoreButtonDialog.create().show();

    }
    public  int getPoint(){
        SharedPreferences sp_confg;
        sp_confg = context.getSharedPreferences("config", MODE_PRIVATE);//confi 文件名
        String userphone=sp_confg.getString("phone","");
        try {
            UserOkHttp.getMyOkHttp().getUserinfo(userphone,sp_confg);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return sp_confg.getInt("point",0);
    }

}
