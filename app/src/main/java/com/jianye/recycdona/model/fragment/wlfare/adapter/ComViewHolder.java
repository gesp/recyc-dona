package com.jianye.recycdona.model.fragment.wlfare.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.jianye.recycdona.R;

public class ComViewHolder extends BaseViewHolder {
    TextView commodiy_price,commdity_name,commdity_baoyou;
    ImageView commodit_imamge;

    public ComViewHolder(View view) {
        super(view);
        commodiy_price=view.findViewById(R.id.commodiy_price);
        commodit_imamge=view.findViewById(R.id.commodit_imamge);
        commdity_name=view.findViewById(R.id.commdity_name);
        commdity_baoyou=view.findViewById(R.id.commdity_baoyou);
    }
}
