package com.jianye.recycdona.model.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.jianye.recycdona.MainActivity;
import com.jianye.recycdona.R;
import com.jianye.recycdona.model.fragment.home.RecyclerpActivity;


public class HomeFragment extends Fragment implements View.OnClickListener {

    public View view;
    public Context context = getActivity();

    //旧衣物回收
    ImageView img_recycler1,img_recycler2;
    //更多
    TextView shouye_tv_gengduo;
    private ImageView tail_iamge1,tail_iamge2;


    public static HomeFragment newInstance(String info) {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        args.putString("info", info);
        fragment.setArguments(args);
        return fragment;
    }



    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view==null){
            view = inflater.inflate(R.layout.fragment_shouye, container, false);
        }

        getActivity().getWindow().setTitle(getArguments().getString("info"));

        initId();


        return view;
    }


    private void initId() {
        //获取旧衣物回收点击ID
        img_recycler1=view.findViewById(R.id.huishou_btn1);
        img_recycler2=view.findViewById(R.id.huishou_btn2);
        //获取旧衣物回收点击ID
        img_recycler1.setOnClickListener(this);
        img_recycler2.setOnClickListener(this);
        //获取 更多> 按钮 点击ID
        shouye_tv_gengduo=view.findViewById(R.id.shouye_tv_gengduo);
        shouye_tv_gengduo.setOnClickListener(this);

        //
        tail_iamge2=view.findViewById(R.id.tail_iamge2);
        tail_iamge1=view.findViewById(R.id.tail_iamge1);






    }

    //获取旧衣物回收点击ID
    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            //获取旧衣物回收点击ID
            case R.id.huishou_btn1:
            case R.id.huishou_btn2:
                startActivity(new Intent(getActivity(), RecyclerpActivity.class));
                break;
            //获取 更多> 按钮 点击ID
            case R.id.shouye_tv_gengduo:
                MainActivity.viewparge.setCurrentItem(2);
                MainActivity.navigationView.getMenu().findItem(R.id.navigation_home).setIcon(R.drawable.img_zhuye);
                MainActivity.navigationView.getMenu().findItem(R.id.navigation_exchange).setIcon(R.drawable.walfare);
                MainActivity.navigationView.getMenu().findItem(R.id.navigation_find).setIcon(R.drawable.img_dongtai2);
                MainActivity.navigationView.getMenu().findItem(R.id.navigation_personal).setIcon(R.drawable.img_geren);
                break;
            default:break;
        }
    }
}

