package com.jianye.recycdona.model.fragment.wlfare.commodityorder.orderfragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.ComOrder;
import com.jianye.recycdona.model.fragment.wlfare.adapter.ComOrderAdapter;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.FragCommodityOderActivity;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * 已发货
 */
public class ShopFinishFragment extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener {





    ListView listViewshopfinish;

    private   View view;
    private ArrayList<ComOrder.DataBean> shopfinish;
    private ComOrderAdapter commoditAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view==null){
            view=inflater.inflate( R.layout.fragment_shop_finish, container, false);
        }

        initData();   //加载数据，初始化数据，初始化对象

        return view;
    }

    public void initData() {

        listViewshopfinish =view.findViewById(R.id.shopfinish_listview);
        shopfinish=new ArrayList<>();
        //适配数据
        //过滤
        for (ComOrder.DataBean c:FragCommodityOderActivity.list) {
            if (c.getStatus().equals("已发货")){
                shopfinish.add(c);
            }
            Log.d(TAG, "loadingDatas: "+c.getStatus());
        }

        //倒序
//        Collections.reverse(shopfinish);
        //设置适配器
        commoditAdapter=new ComOrderAdapter(shopfinish,getContext());
        listViewshopfinish.setAdapter(commoditAdapter);

        listViewshopfinish.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Log.d(TAG, "onItemClick: --------->position"+position);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmreceipt:
                commoditAdapter.notifyDataSetChanged();
                break;
        }
    }
}