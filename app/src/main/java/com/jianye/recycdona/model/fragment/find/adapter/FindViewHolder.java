package com.jianye.recycdona.model.fragment.find.adapter;

import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.jianye.recycdona.R;

public class FindViewHolder extends BaseViewHolder {
    TextView name,decl,lable,content,find_data,comment_number;
    ImageView icon,find_image1,find_image2,find_image3,find_image4,find_image5,find_image6,one_img, Commentcomtent;
    GridLayout gridLayout;
    public FindViewHolder(View view) {
        super(view);

        name = view.findViewById(R.id.tv_find_name);
        decl = view.findViewById(R.id.tv_xuanyan);
        lable = view.findViewById(R.id.tv_label);
        icon=view.findViewById(R.id.img_heade);
        content = view.findViewById(R.id.tv_content);
        gridLayout=view.findViewById(R.id.gridlayout_image);

        find_image1=view.findViewById(R.id.find_image1);
        find_image2=view.findViewById(R.id.find_image2);
        find_image3=view.findViewById(R.id.find_image3);
        find_image4=view.findViewById(R.id.find_image4);
        find_image5=view.findViewById(R.id.find_image5);
        find_image6=view.findViewById(R.id.find_image6);
        one_img=view.findViewById(R.id.one_img);
        Commentcomtent =view.findViewById(R.id.image_comment);

        find_data=view.findViewById(R.id.find_data);//时间
        comment_number=view.findViewById(R.id.comment_number);//评论数
    }
}
