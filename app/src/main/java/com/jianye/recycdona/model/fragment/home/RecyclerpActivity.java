package com.jianye.recycdona.model.fragment.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.jianye.recycdona.R;
import com.jianye.recycdona.model.fragment.mine.FragReseverOrderActivity;

public class RecyclerpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerp);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//竖屏
        ActionBar actionBar=getSupportActionBar();

        if (actionBar!=null){
            actionBar.hide();
        }

    }

    @SuppressLint("NonConstantResourceId")
    public void recyclerOn(View view) {

            startActivity(new Intent(RecyclerpActivity.this,HuishouActivity.class));

    }

    public void origin(View view) {
        startActivity(new Intent(RecyclerpActivity.this, FragReseverOrderActivity.class));
    }


    public void Finish(View view) {
        finish();
    }


}