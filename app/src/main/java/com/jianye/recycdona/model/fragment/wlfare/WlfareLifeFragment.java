package com.jianye.recycdona.model.fragment.wlfare;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.CommoditOrder;
import com.jianye.recycdona.model.fragment.WelfareFragment;
import com.jianye.recycdona.model.fragment.wlfare.adapter.CommoditAdapter;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
//居家

public class WlfareLifeFragment extends Fragment {

    View view;
    List<CommoditOrder.ContentBean> strings=new ArrayList<>();
    RecyclerView recyclerView;
    private CommoditAdapter commoditAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view==null){
            view=inflater.inflate(R.layout.fragment_wlfare_life, container, false);
        }


        initFindviewId( WelfareFragment.getWlfareFragment().getCommdity());
        return view;

    }

    //加载商品数据
    @SuppressLint("ResourceAsColor")
    public void initFindviewId(  List<CommoditOrder.ContentBean> strings) {

        List<CommoditOrder.ContentBean> LifeList=new ArrayList<>();
         this.strings =strings;

        Log.d(TAG, "------------------------->initFindviewId: " + strings.size());
        for ( CommoditOrder.ContentBean list:strings){
            if (list.getCategories().equals("居家")&&list.getStatus().equals("上架")){
                LifeList.add(list);
            }
        }

        commoditAdapter = new CommoditAdapter(R.layout.commodiy_layout, LifeList);
        commoditAdapter.setContext(getContext());

        recyclerView = view.findViewById(R.id.life_recyclerview);
        TextView food=view.findViewById(R.id.food);
        if (LifeList.size()<3){
            food.setVisibility(View.GONE);

        }

        //布局管理器，
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        recyclerView.setAdapter(commoditAdapter);

        //设置 recyclerview 点击事件
        commoditAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                Intent intent=new Intent(getActivity(), CommodityDtailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("name",LifeList.get(position).getProductName());//商品name
                intent.putExtra("id",LifeList.get(position).getId()+"");
                intent.putExtra("specifi",LifeList.get(position).getSpecifi());//商品规格
                intent.putExtra("sold",LifeList.get(position).getProductSold()+"");//商品已售
                intent.putExtra("commcarousel",LifeList.get(position).getCommcarousel());//商品轮播图
                intent.putExtra("produdetails",LifeList.get(position).getProdudetails());//商品详情
                intent.putExtra("price",LifeList.get(position).getPrice()+"");//商品价格
                intent.putExtra("freigh",LifeList.get(position).getFreigh());//运费
                intent.putExtra("insock",LifeList.get(position).getInsock()+"");//库存
                intent.putExtra("pints_redeem",LifeList.get(position).getPintsRedeem()+"");//所需兑换积分
                intent.putExtra("details_map",LifeList.get(position).getDetailsMap());//商品详情图
                startActivity(intent);
            }
        });


    }

}