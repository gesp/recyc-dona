package com.jianye.recycdona.model.fragment.find.adapter;

import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.jianye.recycdona.R;

public class CommentHolder extends BaseViewHolder {

    TextView name,content;
    public CommentHolder(View view) {
        super(view);
        name=view.findViewById(R.id.comment_name);
        content=view.findViewById(R.id.comment_content);
    }
}
