   package com.jianye.recycdona.model.fragment;


   import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.jianye.recycdona.R;
import com.jianye.recycdona.model.fragment.mine.FragReseverOrderActivity;
import com.jianye.recycdona.model.fragment.mine.UserInfoActivity;
   import com.jianye.recycdona.model.fragment.wlfare.commodityorder.FragCommodityOderActivity;
   import com.zhy.autolayout.AutoLinearLayout;
import com.zhy.autolayout.AutoRelativeLayout;

import static android.content.Context.MODE_PRIVATE;


   public class MineFragment extends Fragment implements View.OnClickListener {

    private View view;


       TextView img_username_info2;
       AutoRelativeLayout userinfo;
       AutoLinearLayout mine_wangcheng,mine_yuyue_all,mine_jingxin;
       TextView mine_phone;//手机号
       TextView mine_point;//积分
       SharedPreferences sp_confi;
       private AutoLinearLayout commditOrder;


       public static MineFragment newInstance(String info) {
        Bundle args = new Bundle();
        MineFragment fragment = new MineFragment();
        args.putString("info", info);
        fragment.setArguments(args);
        return fragment;
    }

       @Override
       public void onResume() {
           super.onResume();
           setData();
       }

       public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        getActivity().getWindow().setTitle(getArguments().getString("info"));

        SharedPreferences sp = getActivity().getSharedPreferences("peng", getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        boolean butt = true;
        editor.putBoolean("buttoncf", butt);
        editor.apply();

        view = inflater.inflate(R.layout.fragment_mine, container, false);
        initId();
        setData();
        return view;

    }


       private void setData() {
           sp_confi = getActivity().getSharedPreferences("config", MODE_PRIVATE);//confi 文件名
           //用户名设置
           img_username_info2=view.findViewById(R.id.img_username_info2);
           img_username_info2.setText(sp_confi.getString("username","环保新手"));

           //电话号码设置
           mine_phone=view.findViewById(R.id.mine_phone);
           String phone= sp_confi.getString("phone","");
           String p=phone.trim().substring(0,3)+"****"+phone.trim().substring(7);
           mine_phone.setText(p);

           //积分设置
           mine_point=view.findViewById(R.id.mine_point);
           Integer point=sp_confi.getInt("point",5);
           mine_point.setText(point+"颗");
       }


       @SuppressLint("SetTextI18n")
    private void initId() {

           setData();
           commditOrder=view.findViewById(R.id.commditOrder);
           commditOrder.setOnClickListener(this);
            userinfo=view.findViewById(R.id.info);
            userinfo.setOnClickListener(this);
            img_username_info2.setOnClickListener(this);

        mine_wangcheng=view.findViewById(R.id.mine_wangcheng);
        mine_yuyue_all=view.findViewById(R.id.mine_yuyue_all);
        mine_jingxin=view.findViewById(R.id.mine_jingxin);
        mine_jingxin.setOnClickListener(this);
        mine_yuyue_all.setOnClickListener(this);
        mine_wangcheng.setOnClickListener(this);



    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.info:
            case R.id.img_username_info2:
                startActivity(new Intent(getActivity(), UserInfoActivity.class));break;
            case R.id.mine_wangcheng:
                startActivity(new Intent(getActivity(), FragReseverOrderActivity.class).putExtra("page",3));break;
            case R.id.mine_yuyue_all:
                startActivity(new Intent(getActivity(), FragReseverOrderActivity.class).putExtra("page",1));break;
            case R.id.mine_jingxin:
                startActivity(new Intent(getActivity(), FragReseverOrderActivity.class).putExtra("page",2));break;
            case R.id.commditOrder:
                startActivity(new Intent(getActivity(), FragCommodityOderActivity.class));break;




        }

    }
}
