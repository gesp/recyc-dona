package com.jianye.recycdona.model.fragment.wlfare.commodityorder.orderfragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.ComOrder;
import com.jianye.recycdona.model.fragment.wlfare.adapter.ComOrderAdapter;
import com.jianye.recycdona.model.fragment.wlfare.commodityorder.FragCommodityOderActivity;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;


/**
 *待发货
 */
public class ToShipFragment extends Fragment implements AdapterView.OnItemClickListener {

    ListView toship_listview;

    private   View view;
    private ArrayList<ComOrder.DataBean> toshop;
    private ComOrderAdapter commoditAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view==null){
            view=inflater.inflate( R.layout.fragment_to_ship, container, false);
        }

        initData();   //加载数据，初始化数据，初始化对象

        return view;
    }

    //配置适配器数据
    public void setAdapterData( List<ComOrder.DataBean > list){
        if (toshop.size()>0){
            toshop.clear();
        }

    }

    public void initData() {

        toship_listview =view.findViewById(R.id.toship_listview);
        toshop =new ArrayList<>();

        for (ComOrder.DataBean c: FragCommodityOderActivity.list) {

            if (c.getStatus().equals("待发货")){
                toshop.add(c);
            }
            Log.d(TAG, "loadingDatas: "+c.getStatus());
        }

        commoditAdapter=new ComOrderAdapter(toshop,getContext());
        toship_listview.setAdapter(commoditAdapter);
        toship_listview.setOnItemClickListener(this);


        //去支付Item点击事件
//        commoditAdapter.setOnItemDeleteClickListener(new ComOrderAdapter.onItemDeleteListener() {
//            @Override
//            public void onDeleteClick(int i) {
//
////                if (toshop.size()>0){
////                    toshop.clear();
////                }
////
////                //重新获取数据
////                List<ComOrder.DataBean > list = null;
////                try {
////                    list= CommoditOrderHttp.getMyOkHttp().getAllComOrder(Integer.parseInt(FragCommodityOderActivity.parentId));
////                } catch (ExecutionException | InterruptedException e) {
////                    e.printStackTrace();
////                }
////
////                //数据过滤
////                assert list != null;
////                for (ComOrder.DataBean c: list) {
////                    if (c.getStatus().equals("待发货")){
////                        toshop.add(c);
////                    }
////                    Log.d(TAG, "loadingDatas: "+c.getStatus());
////                }
//
//                //倒序
////            Collections.reverse(tranfinish);
//                commoditAdapter.notifyDataSetChanged();
//            }
//        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Log.d(TAG, "onItemClick: --------->position"+position);

    }
}