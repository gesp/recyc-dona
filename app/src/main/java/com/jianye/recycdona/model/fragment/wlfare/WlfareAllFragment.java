package com.jianye.recycdona.model.fragment.wlfare;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.CommoditOrder;
import com.jianye.recycdona.model.fragment.WelfareFragment;
import com.jianye.recycdona.model.fragment.wlfare.adapter.CommoditAdapter;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;


public class WlfareAllFragment extends Fragment {

    View view;
    public List<CommoditOrder.ContentBean>  strings=new ArrayList<>();
    RecyclerView recyclerView;
    private CommoditAdapter commoditAdapter;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view==null){
            view=inflater.inflate(R.layout.fragment_wlfare_all, container, false);
        }

        initFindviewId(WelfareFragment.getWlfareFragment().getCommdity());

        return view;
    }

    //加载商品数据
    public void initFindviewId(List<CommoditOrder.ContentBean> strings) {

        this.strings =strings;
        //数据源
        List<CommoditOrder.ContentBean> data=new ArrayList<>();
        recyclerView = view.findViewById(R.id.wlfareall_recyclerview);

        //过滤
        for (CommoditOrder.ContentBean contentBean:strings) {
            if (contentBean.getStatus().equals("上架")){
                data.add(contentBean);
            }
        }

        Log.d(TAG, "------------------------->initFindviewId: " + data.size());
        commoditAdapter = new CommoditAdapter(R.layout.commodiy_layout, data);
        commoditAdapter.setContext(getContext());


        //布局管理器，
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(commoditAdapter);

        //设置 recyclerview 点击事件
        commoditAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                Intent intent=new Intent(getActivity(), CommodityDtailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("name",data.get(position).getProductName());//商品name
                intent.putExtra("id",data.get(position).getId()+"");
                intent.putExtra("sold",data.get(position).getProductSold()+"");//商品已售
                intent.putExtra("specifi",data.get(position).getSpecifi());//商品规格
                intent.putExtra("commcarousel",data.get(position).getCommcarousel());//商品轮播图
                intent.putExtra("produdetails",data.get(position).getProdudetails());//商品详情
                intent.putExtra("price",data.get(position).getPrice()+"");//商品价格
                intent.putExtra("freigh",data.get(position).getFreigh());//运费
                intent.putExtra("insock",data.get(position).getInsock()+"");//库存
                intent.putExtra("pints_redeem",data.get(position).getPintsRedeem()+"");//所需兑换积分
                intent.putExtra("details_map",data.get(position).getDetailsMap());//商品详情图
                startActivity(intent);
            }
        });


    }


}