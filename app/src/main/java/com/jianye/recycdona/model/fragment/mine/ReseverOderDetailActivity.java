package com.jianye.recycdona.model.fragment.mine;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.jianye.recycdona.R;

public class ReseverOderDetailActivity extends AppCompatActivity {


    private String weight;
    private String ordernum;
    private String orderTime;
    private String reserveStatus;
    private String orderinfo;
    private TextView tv_detail_reserveStatus,name,detai;
    private TextView tv_weight,tv_odernum;
    private TextView tv_detail_planceTime;
    private TextView tv_reserveTime;
    private String reserveTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oder_detail);

        initFindId();
        initFindData();

    }

    private void initFindId() {
        tv_detail_reserveStatus=findViewById(R.id.tv_detail_reserveStatus);
        name=findViewById(R.id.tv_detail_name);
        detai=findViewById(R.id.tv_detail_addressDetail);
        tv_weight=findViewById(R.id.tv_detail_weight);
        tv_reserveTime=findViewById(R.id.tv_detail_reserveTime);
        tv_odernum=findViewById(R.id.tv_detail_orderNum);
        tv_detail_planceTime=findViewById(R.id.tv_detail_planceTime);

    }


    private void initFindData() {

        Intent intent=getIntent();
        reserveStatus=intent.getStringExtra("reserveStatus");
        orderinfo = intent.getStringExtra("orderinfo");
        String[] namephone=orderinfo.split("\n");

        weight=intent.getStringExtra("weight");
        ordernum=intent.getStringExtra("ordernum");
        orderTime=intent.getStringExtra("orderTime");
        reserveTime=intent.getStringExtra("reserveTime");
        Log.d("", "initFindData: "+reserveStatus+orderinfo+weight+ordernum+orderTime);

        tv_detail_reserveStatus.setText(reserveStatus);
        name.setText(namephone[0]);
        detai.setText(namephone[1]);
        tv_weight.setText(weight);
        tv_odernum.setText(ordernum);
        tv_reserveTime.setText(reserveTime);
        tv_detail_planceTime.setText(orderTime);


    }
}