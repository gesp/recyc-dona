package com.jianye.recycdona.model.fragment.mine.listviewAapater;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.jianye.recycdona.R;
import com.jianye.recycdona.control.bean.RecyclerOrder;
import com.jianye.recycdona.control.http.OrderHttp;
import com.jianye.recycdona.model.fragment.home.HuishouActivity;
import com.jianye.recycdona.model.fragment.mine.ReseverOderDetailActivity;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class OrderAdapater extends BaseAdapter {

    Context context;
    List<RecyclerOrder.DataBean> list;
    Integer flag;
    ViewHolder viewHolder;

    String reserveFlag;

    public OrderAdapater(Context context, List<RecyclerOrder.DataBean> list, Integer flag) {
        this.context = context;
        this.list = list;
        this.flag=flag;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {



        if(convertView == null){

            viewHolder = new ViewHolder();

            convertView= LayoutInflater.from(context).inflate(R.layout.oder_layout_list, parent,false);
            viewHolder.ordernumber = convertView.findViewById(R.id.tv_order_num);//订单号
            viewHolder.reserveStatus = convertView.findViewById(R.id.tv_order_reserveStatus);//预约状态
            viewHolder.weight = convertView.findViewById(R.id.tv_order_weight);//衣物重量
            viewHolder.reserveTime = convertView.findViewById(R.id.tv_oder_reserveTime);//取件日期
            viewHolder.oderTime=convertView.findViewById(R.id.tv_oder_oderTime);//下单时间
            viewHolder.change=convertView.findViewById(R.id.tv_order_change);//取消预约
            viewHolder.edit =convertView.findViewById(R.id.tv_order_ReviseOder);//修改订单
            viewHolder.detail=convertView.findViewById(R.id.tv_order_Details);//订单详情

            convertView.setTag(viewHolder);//讲ViewHolder存储在View中

        }else{
            viewHolder =(ViewHolder) convertView.getTag();//重获取viewHolder
        }

//        reserveFlag=viewHolder.change.getText().toString().trim();

        viewHolder.ordernumber.setText(list.get(position).getOrderNumber());
        viewHolder.reserveStatus.setText(list.get(position).getReserveStatus());
        viewHolder.weight.setText(list.get(position).getWeight());
        viewHolder.reserveTime.setText(list.get(position).getReserveTime());
        viewHolder.oderTime.setText(list.get(position).getPlaceTime());

        //点击订单详情
        viewHolder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, ReseverOderDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("reserveStatus",list.get(position).getReserveStatus());
                intent.putExtra("orderinfo",list.get(position).getOrderinfo());
                intent.putExtra("weight",list.get(position).getWeight());
                intent.putExtra("reserveTime",list.get(position).getReserveTime());
                intent.putExtra("ordernum",list.get(position).getOrderNumber());
                intent.putExtra("orderTime",list.get(position).getPlaceTime());

                context.startActivity(intent);

            }
        });



        //点击取消预约
        viewHolder.change.setOnClickListener(new View.OnClickListener() {

            String code;
            @Override
            public void onClick(View v) {

                if ( list.get(position).getReserveStatus().equals("预约取消")){
                    Intent intent=new Intent(context, HuishouActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(new Intent(intent));
                }
                //发起请求
                if (!list.get(position).getReserveStatus().equals("预约取消")){
                     code=OrderHttp.getMyOkHttp().updataOderStatus(list.get(position).getId() + "", "已取消", "预约取消");

                    if (code.equals("200")){
                        viewHolder.change.setText("重新预约");
                        Toasty.success(context, "取消成功,刷新一下", Toast.LENGTH_SHORT, true).show();
                        viewHolder.edit.setVisibility(View.INVISIBLE);//修改订单

                      //  notifyDataSetInvalidated();
                        //notifyDataSetChanged();//重新调用gitview

                    }


                }

//                notifyDataSetInvalidated();

            }

        });


        //修改订单
        viewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    Intent intent = new Intent(context, HuishouActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("edit", "0");
                    intent.putExtra("id", list.get(position).getId() + "");
                    intent.putExtra("editdetail", list.get(position).getOrderinfo());
                    intent.putExtra("editreserveTime", list.get(position).getReserveTime());
                    intent.putExtra("editweight", list.get(position).getWeight());
                    context.startActivity(intent);
//                notifyDataSetInvalidated();
            }
        });


        //取消预约后 页面过滤
        if(list.get(position).getReserveStatus().equals("预约取消")) {
            viewHolder.change.setText("重新预约");
            viewHolder.edit.setVisibility(View.INVISIBLE);//隐藏修改订单
        }



        //对所有预约进行过滤
        if (list.get(position).getOrderStatus().equals("已完成")||list.get(position).getReserveStatus().equals("已取件")){

            if (list.get(position).getOrderStatus().equals("已完成")){//订单状态为已完成
                viewHolder.reserveStatus.setText("已完成");//将预约状态设置为已完成进行显示
            }
            viewHolder.edit.setVisibility(View.INVISIBLE);//修改订单
            viewHolder.change.setVisibility(View.INVISIBLE);//取消预约

        }


        //对进行中进行过滤
        if (flag==2){//进行中
            if (list .get(position).getOrderStatus().equals("进行中")&&list.get(position).getReserveStatus().equals("已取件")||
            list.get(position).getReserveStatus().equals("等待取件")){

                if (list.get(position).getReserveStatus().equals("已取件")){
                    viewHolder.edit.setVisibility(View.INVISIBLE);//修改订单
                    viewHolder.change.setVisibility(View.INVISIBLE);//取消预约
                }

            }
        }


        //已完成页面过滤
        if (flag==3){//已完成页面
            //订单状态为已完成，就将预约状态设置为已完成进行显示
            if (list.get(position).getOrderStatus().equals("已完成")&&list.get(position).getReserveStatus().equals("已取件")){
                viewHolder.reserveStatus.setText("已完成");
                viewHolder.edit.setVisibility(View.INVISIBLE);//修改订单
                viewHolder.change.setVisibility(View.INVISIBLE);//取消预约
            }
        }

        return convertView;
    }


    public static class ViewHolder{
        TextView ordernumber,reserveStatus,weight,reserveTime,oderTime, edit,change,detail;
    }

}
