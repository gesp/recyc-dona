package com.jianye.recycdona.model.fragment.mine;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.jianye.recycdona.R;
import com.jianye.recycdona.control.http.UserOkHttp;
import com.jianye.recycdona.model.utils.CheckNetWork;

import java.util.concurrent.ExecutionException;

import static com.jianye.recycdona.MainActivity.linearLayout_home;
import static com.jianye.recycdona.MainActivity.linearLayout_login;

public class UserInfoActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_exit_view;//退出账号
    RadioGroup radioGroup;//男/女
    RadioButton radioButton0;
    RadioButton radioButton1;
    private SharedPreferences sp;
    TextView textView_phone2;
    TextView tv_username;//用户名

    UserOkHttp myOkHttp;
    private String sex;
    public  String id;

    public  Thread thread;

    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//竖屏
        ActionBar actionBar=getSupportActionBar();

        if (actionBar!=null){
            actionBar.hide();
        }

        initById();
        initHTTP();
        initSetdata();
    }

    //ID获取
    private void initById() {
        tv_exit_view=findViewById(R.id.tv_exit);
        tv_exit_view.setOnClickListener(this);
        radioGroup=findViewById(R.id.radio_group);

        textView_phone2=findViewById(R.id.tv_phone2);
        tv_username=findViewById(R.id.tv_username);

        radioButton0=findViewById(R.id.radiobut0);
        radioButton1=findViewById(R.id.radiobut1);
    }


    private void initSetdata() {

        //设置用户名
        tv_username.setText(sp.getString("username","环保新手"));
        //获取ID,性别
        id=sp.getString("id","");
        sex=sp.getString("sex","");

        if(sex.equals("男")){
            //男
            radioGroup.check(R.id.radiobut1);
        }else if (sex.equals("女")){
            //女
            radioGroup.check(R.id.radiobut0);
        }


//        Log.e("putid2",sp.getInt("id",9)+"");
//        Log.e("putusername2",sp.getString("username",""));
//        Log.e("putpoint2",sp.getInt("point",9)+"");

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                selectRadioBtn();
            }
        });

    }

    /**
     * 发送请求获取数据
     * config
     */
    private void initHTTP() {
        sp = getSharedPreferences("config", MODE_PRIVATE);//confi 文件名
        String phone=sp.getString("phone","");
        String p=phone.trim().substring(0,3)+"****"+phone.trim().substring(7);
        //设置手机号
        textView_phone2.setText(p);

        if (!CheckNetWork.checkConnectNetwork(UserInfoActivity.this)) {
            Toast.makeText(this, "请检查网络", Toast.LENGTH_SHORT).show();
        }


        int code= 0;
        try {
            code = UserOkHttp.getMyOkHttp().getUserinfo(phone,sp);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        Log.e("code/**/",""+code);






    }


    //查询用户选择的性别  1=男，2=女
    private void selectRadioBtn() {

        RadioButton rb = UserInfoActivity.this.findViewById(radioGroup.getCheckedRadioButtonId());

        if (!CheckNetWork.checkConnectNetwork(UserInfoActivity.this)) {
            Toast.makeText(this, "请检查网络", Toast.LENGTH_SHORT).show();
        } else {

            if (rb.getText().equals("男")){
                radioGroup.check(R.id.radiobut1);

                try {
                    UserOkHttp.getMyOkHttp().Updateinfo(id+"","sex","男");//男

                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
                //加载到本地数据库
                editor=sp.edit();
                editor.putString("sex","男");
                editor.apply();

                Toast.makeText(UserInfoActivity.this, "修改完成", Toast.LENGTH_SHORT).show();
            }else {

                radioGroup.check(R.id.radiobut0);
                try {
                    UserOkHttp.getMyOkHttp().Updateinfo(id+"","sex","女");//女
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
                //加载到本地数据库
                editor=sp.edit();
                editor.putString("sex","女");
                editor.apply();
                Toast.makeText(UserInfoActivity.this, "修改完成", Toast.LENGTH_SHORT).show();
            }
        }




    }

    //事件监听
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            //退出账号
            case R.id.tv_exit:
                linearLayout_login.setVisibility(View.VISIBLE);//显示
                linearLayout_home.setVisibility(View.GONE);//隐藏
                finish();
                break;
        }
    }

    //点击用户名
    public void username(View view) {
        showInputDialog();
    }


    //修改用户名
    private void showInputDialog() {
        /*@setView 装入一个EditView
         */
        if (!CheckNetWork.checkConnectNetwork(UserInfoActivity.this)) {
            Toast.makeText(UserInfoActivity.this, "请检查网络", Toast.LENGTH_SHORT).show();
        }else {

            final EditText editText = new EditText(UserInfoActivity.this);
            AlertDialog.Builder inputDialog = new AlertDialog.Builder(UserInfoActivity.this);
            inputDialog.setTitle("请输入新的用户名").setView(editText);

            inputDialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String username = editText.getText().toString().trim();

                    if (!username.equals("")) {

                        try {
                            UserOkHttp.getMyOkHttp().Updateinfo(id + "", "username", username);
                        } catch (ExecutionException | InterruptedException e) {
                            e.printStackTrace();
                        }

                        tv_username.setText(username);

                        editor=sp.edit();
                        editor.putString("username",username);
                        editor.apply();


                        Toast.makeText(UserInfoActivity.this, "修改完成", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(UserInfoActivity.this, "用户名不能为空", Toast.LENGTH_SHORT).show();
                    }


                }
            }).show();

        }
    }


    public void Finish(View view) {
        finish();
    }

    //修改手机号
//    public void updatePhone(View view) {
//        /*@setView 装入一个EditView
//         */
//
//        if (!CheckNetWork.checkConnectNetwork(UserInfoActivity.this)) {
//            Toast.makeText(UserInfoActivity.this, "请检查网络", Toast.LENGTH_SHORT).show();
//        }else {
//        final EditText editText = new EditText(UserInfoActivity.this);
//        AlertDialog.Builder inputDialog = new AlertDialog.Builder(UserInfoActivity.this);
//        inputDialog.setTitle("请输入新的手机号").setView(editText);
//
//        inputDialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//
//                String phone=editText.getText().toString().trim();
//
//                if (phone.equals("")){
//                    Toast.makeText(UserInfoActivity.this, "手机号不能为空", Toast.LENGTH_SHORT).show();
//                }else if (!CheckNetWork.isValidPhoneNumber(phone)) {
//                    Toast.makeText(UserInfoActivity.this, "手机格式不正确", Toast.LENGTH_SHORT).show();
//                }else{
//                    //检查手机号是否已经注册
//                    try {
//                        UserOkHttp.getMyOkHttp().chekPhone(phone);
//                    } catch (ExecutionException | InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
////                    MyOkHttp.getMyOkHttp().checkPhoneReginster(phone);
//                }
//                if (UserOkHttp.getMyOkHttp().getResponseMsg().equals("1"))
//                {
//                    Toast.makeText(UserInfoActivity.this, "手机号已经存在", Toast.LENGTH_SHORT).show();
//                }else {
//
//                    try {
//                        UserOkHttp.getMyOkHttp().Updateinfo(id+"","phone",phone);
//                    } catch (ExecutionException | InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    textView_phone2.setText(phone);
//
//                    editor=sp.edit();
//                    editor.putString("phone",phone);
//                    editor.apply();
//                    Toast.makeText(UserInfoActivity.this, "修改完成", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        }).show();
//
//
//        }
//    }
}