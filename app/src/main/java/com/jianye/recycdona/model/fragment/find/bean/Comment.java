package com.jianye.recycdona.model.fragment.find.bean;

import java.util.List;


public class Comment {
    private List<DataBean> data;
    private String msg;
    private Integer status;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public static class DataBean {
        private String cmtcontent;
        private String comentinfo;
        private String cretedata;
        private Integer findid;
        private Integer id;
        private String name;
        private Integer parenid;
        private Integer thumbsnum;

        public String getCmtcontent() {
            return cmtcontent;
        }

        public void setCmtcontent(String cmtcontent) {
            this.cmtcontent = cmtcontent;
        }

        public String getComentinfo() {
            return comentinfo;
        }

        public void setComentinfo(String comentinfo) {
            this.comentinfo = comentinfo;
        }

        public String getCretedata() {
            return cretedata;
        }

        public void setCretedata(String cretedata) {
            this.cretedata = cretedata;
        }

        public Integer getFindid() {
            return findid;
        }

        public void setFindid(Integer findid) {
            this.findid = findid;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getParenid() {
            return parenid;
        }

        public void setParenid(Integer parenid) {
            this.parenid = parenid;
        }

        public Integer getThumbsnum() {
            return thumbsnum;
        }

        public void setThumbsnum(Integer thumbsnum) {
            this.thumbsnum = thumbsnum;
        }
    }
}
