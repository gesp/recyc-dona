package com.jianye.recycdona.model.fragment.home.bean;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jianye.recycdona.R;
import com.jianye.recycdona.model.fragment.home.Add_addressInfoActivity;

import java.util.List;

public class AddressInfoAdapter extends BaseAdapter {

    Context context;
    List<addressInfo.DataBean> list;





    public AddressInfoAdapter(List<addressInfo.DataBean> list,Context context) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView == null){

            viewHolder = new ViewHolder();

            convertView=LayoutInflater.from(context).inflate(R.layout.address_layout_list, parent,false);
            viewHolder.name = convertView.findViewById(R.id.tv_address_name);
            viewHolder.phone = convertView.findViewById(R.id.tv_address_phone);
            viewHolder.detai = convertView.findViewById(R.id.tv_address_detai);
            viewHolder.edit = convertView.findViewById(R.id.tv_address_edit);
            viewHolder.id=convertView.findViewById(R.id.addressId);

            convertView.setTag(viewHolder);//讲ViewHolder存储在View中

        }else{
            viewHolder =(ViewHolder) convertView.getTag();//重获取viewHolder
        }

        viewHolder.name.setText(list.get(position).getReceiver());
        viewHolder.phone.setText(list.get(position).getPhone());
        viewHolder.detai.setText(list.get(position).getAddress()+list.get(position).getDetaiAddress());
        viewHolder.id.setText(list.get(position).getId()+"");//地址id,默认为隐藏状态
        viewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, Add_addressInfoActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("status","updata");
                intent.putExtra("id",list.get(position).getId());
                intent.putExtra("name",list.get(position).getReceiver());
                intent.putExtra("phone",list.get(position).getPhone());
                intent.putExtra("city",list.get(position).getAddress());
                intent.putExtra("detai",list.get(position).getDetaiAddress());


                context.startActivity(intent);
            }
        });

        return convertView;
    }


    static class ViewHolder{
        TextView name,phone,detai,edit,id;
    }
}
