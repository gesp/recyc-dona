package com.jianye.recycdona.control.http;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.jianye.recycdona.control.Apps;
import com.jianye.recycdona.control.bean.ComOrder;
import com.jianye.recycdona.control.bean.CommoditOrder;
import com.jianye.recycdona.model.fragment.find.bean.Find;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.content.ContentValues.TAG;
import static com.jianye.recycdona.control.Apps.JSON;


public class CommoditOrderHttp {

    private static final CommoditOrderHttp creaditsHttp=new CommoditOrderHttp();

    private final Gson gson=common.getGson();
    private ArrayList<Find.ContentBean> recyclerFindList;

    public static String getCodeStatus() {
        return codeStatus;
    }

    public static  String codeStatus;
    private    static final OkHttpClient client= common.getClient();
    private List<CommoditOrder.ContentBean> contentBeans;

    private List<ComOrder.DataBean> comorderList;


    public static CommoditOrderHttp getMyOkHttp() {
        return creaditsHttp;
    }

    private CommoditOrderHttp() {
    }

    /**
     * 获取商品数据
     */
    public List<CommoditOrder.ContentBean> getCommoditOrder() throws ExecutionException, InterruptedException {
        contentBeans=new ArrayList<>();
        CommoditOrderHP callableThread = new CommoditOrderHP();
        FutureTask futureTask = new FutureTask<>(callableThread);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
        return contentBeans;
    }

    class CommoditOrderHP implements Callable {

        @Override
        public Object call() throws Exception {


            Request request = new Request.Builder()
                    .url(Apps.getcommodityManage)//请求接口。如果需要传参拼接到接口后面。
                    .build();//创建Request 对象
            Response response = null;
            response = client.newCall(request).execute();//得到Response 对象

            if (response.isSuccessful()) {

                try {
                    assert response.body() != null;
                    String msginfo = response.body().string();

                    CommoditOrder fromJson = gson.fromJson(msginfo, CommoditOrder.class);

                    int listsize=fromJson.getContent().size();
//                    contentBeans=fromJson.getContent();
//
                    for(int i=0;i<listsize;i++) {
                        contentBeans.add(fromJson.getContent().get(i));
                        Log.d("comData", "comData: ----------------->"+fromJson.getContent().get(i).getCategories());
                    }

                    Log.d("commoditlength", "comData:----------------->"+listsize);



                } catch (IOException | JsonSyntaxException e) {
                    e.printStackTrace();
                }

            }

            return 200;
        }
    }

    /**
     * 提交订单到后台
     */
    public String postCommdityOrder(  String addressinfo, String addressId, String createDate, String freight, String nunber, String commid,
                                       String orderinfo, String ordernum, Integer parentId, Double price, String status, String paidpoint
    ) throws ExecutionException, InterruptedException {


        postCommdityOrderHttp callableThread = new postCommdityOrderHttp(  addressinfo, addressId, createDate, freight,
                nunber, commid, orderinfo, ordernum, parentId, price, status, paidpoint);

        FutureTask futureTask = new FutureTask<>(callableThread);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
        return codeStatus;
    }

    class postCommdityOrderHttp implements Callable {

        String addressinfo;
        String addressId;
        String createDate;
        String freight;
        String nunber;
        String commid;
        String orderinfo;
        String ordernum;
        Integer parentId;
        Double price;
        String status;
        String paidpoint;

        public postCommdityOrderHttp(String addressinfo, String addressId, String createDate, String freight, String nunber,
                                     String commid, String orderinfo, String ordernum, Integer parentId, Double price,
                                     String status, String paidpoint)
        {
            this.addressinfo = addressinfo;
            this.addressId = addressId;
            this.createDate = createDate;
            this.freight = freight;
            this.nunber = nunber;
            this.commid = commid;
            this.orderinfo = orderinfo;
            this.ordernum = ordernum;
            this.parentId = parentId;
            this.price = price;
            this.status = status;
            this.paidpoint = paidpoint;
        }

        @Override
        public Object call() throws Exception {
            System.out.println(Thread.currentThread().getName());

            try {
                //传递json
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("parent_id", parentId);
                jsonObject.put("commid", commid);//收货人
                jsonObject.put("address_id", addressId);
                jsonObject.put("addressinfo", addressinfo);
                jsonObject.put("ordernum", ordernum);

                jsonObject.put("status", status);
                jsonObject.put("orderinfo", orderinfo);//收货人
                jsonObject.put("nunber", nunber);
                jsonObject.put("freight", freight);
                jsonObject.put("price", price);
                jsonObject.put("create_date", createDate);
                //实付积分
                jsonObject.put("paidpoint", paidpoint);


                String callStr = UserOkHttp.post(Apps.postCommdityOrder, jsonObject.toString());
                Log.d(TAG, "jsonObject.toString()= "+jsonObject.toString());
                JSONObject call_json = new JSONObject(callStr);
                 codeStatus = call_json.getString("status");
                Log.d(TAG, "--------------->call: "+codeStatus);

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            System.out.print("-------------------------------OK");
            return "ok";
        }

    }


    /**
     * Get 获取所有用户订单数据
     */
    public List<ComOrder.DataBean> getAllComOrder(Integer parentId) throws ExecutionException, InterruptedException {
        comorderList=new ArrayList<>();
        ComOrderHttp callableThread = new ComOrderHttp(parentId);
        FutureTask futureTask = new FutureTask<>(callableThread);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
        return comorderList;
    }

    class ComOrderHttp implements Callable {

        Integer parentId;

        public ComOrderHttp(Integer parentId) {
            this.parentId = parentId;
        }

        @Override
        public Object call() throws Exception {


            Request request = new Request.Builder()
                    .url(Apps.getCommdityOrder+parentId)//请求接口。如果需要传参拼接到接口后面。
                    .build();//创建Request 对象
            Response response = null;
            response = client.newCall(request).execute();//得到Response 对象

            if (response.isSuccessful()) {

                try {
                    assert response.body() != null;
                    String msginfo = response.body().string();

                    ComOrder fromJson = gson.fromJson(msginfo, ComOrder.class);
                    codeStatus=fromJson.getStatus()+"";
                    int listsize=fromJson.getData().size();
//                    contentBeans=fromJson.getContent();
//
                    for(int i=0;i<listsize;i++) {
                        comorderList.add(fromJson.getData().get(i));
                        Log.d("commoditData", "commoditData: ----------------->"+fromJson.getData().get(i).getId());
                    }
                    Log.d("commoditlength", "call: ----------------->"+listsize);

                } catch (IOException | JsonSyntaxException e) {
                    e.printStackTrace();
                }

            }

            return codeStatus;
        }
    }


    /**
     * 修改订单状态
     * 用户id
     * Status
     */

    public String putCommdityOrder(String id,String status) throws ExecutionException, InterruptedException {
        putCommdityOrderHttp callableThread = new putCommdityOrderHttp(id,status);
        FutureTask futureTask = new FutureTask<>(callableThread);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
        return codeStatus;
    }

    class putCommdityOrderHttp implements Callable {
        private String id;
        private String status;
        public putCommdityOrderHttp(String id, String status) {
            this.id = id;
            this.status = status;
        }

        @Override
        public Object call() throws Exception {

            //传的json
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", id+"");
            jsonObject.put( "status", status);

            String callStr = put(jsonObject.toString());
            JSONObject call_json = new JSONObject(callStr);
            final String code = call_json.getString("status");
            codeStatus=code;
            Log.e("putCommdityOrderHttp", code + "-------------------------------code");

            System.out.println(Thread.currentThread().getName());
            return codeStatus;
        }

    }

    public   String put(String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(Apps.putCommdityOrder)
                .put(body)
                .build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            assert response.body() != null;
            return response.body().string();
        } else {
            throw new IOException("Unexpected code " + response);
        }

    }

    /**
     * 删除数据 根据ID删除
     * @return
     * @param id
     */

    public String deleteCommdityOrder(String id){
        deleteCommdityOrder okHttpDelete=new deleteCommdityOrder(id);
        FutureTask futureTask = new FutureTask<>(okHttpDelete);
        new Thread(futureTask).start();
        try {
            System.out.println(futureTask.get());
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return codeStatus;
    }

    static class deleteCommdityOrder implements Callable {

        String id;
        public deleteCommdityOrder(String id) {
            this.id = id;
        }
        @Override
        public Object call() throws Exception {

            Request request = new Request.Builder().url(Apps.deleteCommdityOrder+id).delete().build();
            Log.e("Delete", "--------------------------->"+Apps.deleteCommdityOrder+id);
            try (Response response = client.newCall(request).execute()) {
                assert response.body() != null;
                String msg=response.body().string();
                Log.e("Delete", "--------------------------->"+msg);
                return msg;
            }

        }
    }


    /**
     * 获取商品数据
     */
    public List<Find.ContentBean> getrecyclerFind() throws ExecutionException, InterruptedException {
        recyclerFindList=new ArrayList<>();
        getrecyclerFind callableThread = new getrecyclerFind();
        FutureTask futureTask = new FutureTask<>(callableThread);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
        return recyclerFindList;
    }

    class getrecyclerFind implements Callable {

        @Override
        public Object call() throws Exception {


            Request request = new Request.Builder()
                    .url(Apps.getrecyclerFind)//请求接口。如果需要传参拼接到接口后面。
                    .build();//创建Request 对象
            Response response = null;
            response = client.newCall(request).execute();//得到Response 对象

            if (response.isSuccessful()) {

                try {
                    assert response.body() != null;
                    String msginfo = response.body().string();

                    Find fromJson = gson.fromJson(msginfo, Find.class);

                    int listsize=fromJson.getContent().size();

                    for(int i=0;i<listsize;i++) {
                        recyclerFindList.add(fromJson.getContent().get(i));
                        Log.d("Find", "Find: ----------------->"+fromJson.getContent().get(i).getImages());
                    }

                    Log.d("Findlength", "Findlength:----------------->"+listsize);

                } catch (IOException | JsonSyntaxException e) {
                    e.printStackTrace();
                }

            }

            return 200;
        }
    }




    /**
     * 提交商品反馈信息
     */
    public String postShopCommentComment(  String userid, String shopid, String name, String content, String createdata, String shopobject,
                                      String orderinfo
    ) throws ExecutionException, InterruptedException {


        postShopCommentCommentHttp callableThread = new postShopCommentCommentHttp(  userid, shopid, name, content,
                createdata,  shopobject,  orderinfo);

        FutureTask futureTask = new FutureTask<>(callableThread);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
        return codeStatus;
    }

    class postShopCommentCommentHttp implements Callable {

        String userid;
        String shopid;
        String name;
        String content;
        String createdata;
        String shopobject;
        String orderinfo;


        public postShopCommentCommentHttp( String userid, String shopid, String name, String content,
                                           String createdata, String shopobject, String orderinfo)
        {
            this.userid = userid;
            this.shopid = shopid;
            this.name = name;
            this.content = content;
            this.createdata = createdata;
            this.shopobject = shopobject;
            this.orderinfo = orderinfo;
        }

        @Override
        public Object call() throws Exception {
            System.out.println(Thread.currentThread().getName());

            try {
                //传递json
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userid", userid);
                jsonObject.put("shopid", shopid);//收货人
                jsonObject.put("name", name);
                jsonObject.put("content", content);
                jsonObject.put("createdata", createdata);
                jsonObject.put("shopobject", shopobject);
                jsonObject.put("orderinfo", orderinfo);//收货人


                String callStr = UserOkHttp.post(Apps.postShopCommentComment, jsonObject.toString());
                Log.d(TAG, "jsonObject.toString()= "+jsonObject.toString());
                JSONObject call_json = new JSONObject(callStr);
                codeStatus = call_json.getString("status");
                Log.d(TAG, "--------------->call: "+codeStatus);

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            System.out.print("-------------------------------OK");
            return "ok";
        }

    }



}
