package com.jianye.recycdona.control.bean;

import java.util.List;


public class RecyclerOrder {
    static final long serialVersionUID = 42L;
    private List<DataBean> data;
    private String msg;
    private Integer status;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static class DataBean {
        private Integer addressid;
        private Integer id;
        private String orderNumber;
        private String orderStatus;//订单状态
        private String orderinfo;
        private Integer parentId;
        private String placeTime;
        private String reserveStatus;
        private String reserveTime;
        private String weight;

        public DataBean(Integer addressid, Integer id, String orderNumber, String orderStatus, String orderinfo, Integer parentId, String placeTime, String reserveStatus, String reserveTime, String weight) {
            this.addressid = addressid;
            this.id = id;
            this.orderNumber = orderNumber;
            this.orderStatus = orderStatus;
            this.orderinfo = orderinfo;
            this.parentId = parentId;
            this.placeTime = placeTime;
            this.reserveStatus = reserveStatus;
            this.reserveTime = reserveTime;
            this.weight = weight;
        }

        public Integer getAddressid() {
            return addressid;
        }

        public void setAddressid(Integer addressid) {
            this.addressid = addressid;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getOrderNumber() {
            return orderNumber;
        }

        public void setOrderNumber(String orderNumber) {
            this.orderNumber = orderNumber;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getOrderinfo() {
            return orderinfo;
        }

        public void setOrderinfo(String orderinfo) {
            this.orderinfo = orderinfo;
        }

        public Integer getParentId() {
            return parentId;
        }

        public void setParentId(Integer parentId) {
            this.parentId = parentId;
        }

        public String getPlaceTime() {
            return placeTime;
        }

        public void setPlaceTime(String placeTime) {
            this.placeTime = placeTime;
        }

        public String getReserveStatus() {
            return reserveStatus;
        }

        public void setReserveStatus(String reserveStatus) {
            this.reserveStatus = reserveStatus;
        }

        public String getReserveTime() {
            return reserveTime;
        }

        public void setReserveTime(String reserveTime) {
            this.reserveTime = reserveTime;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }
    }
}
