package com.jianye.recycdona.control.bean;

import com.stx.xhb.xbanner.entity.SimpleBannerInfo;

import java.util.List;


public class Carousel {

    private List<ContentBean> content;
    private Integer totalElements;

    public List<ContentBean> getContent() {
        return content;
    }

    public void setContent(List<ContentBean> content) {
        this.content = content;
    }

    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    public static class ContentBean extends SimpleBannerInfo {
        private String carousel;
        private Integer id;

        public ContentBean(String carousel, Integer id) {
            this.carousel = carousel;
            this.id = id;
        }

        public String getCarousel() {
            return carousel;
        }

        public void setCarousel(String carousel) {
            this.carousel = carousel;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        @Override
        public Object getXBannerUrl() {
            return getCarousel();
        }
    }
}
