package com.jianye.recycdona.control.http;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

import static com.jianye.recycdona.control.http.UserOkHttp.CONNECT_TIMEOUT;
import static com.jianye.recycdona.control.http.UserOkHttp.READ_TIMEOUT;
import static com.jianye.recycdona.control.http.UserOkHttp.WRITE_TIMEOUT;

public class common {

    private    static OkHttpClient client= new OkHttpClient.Builder()
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)//设置读取超时时间
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)//设置写的超时时间
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)//设置连接超时时间
            .build();

    private final static Gson gson=new Gson();

    public static OkHttpClient getClient() {
        return client;
    }

    public static  Gson getGson() {
        return gson;
    }


}
