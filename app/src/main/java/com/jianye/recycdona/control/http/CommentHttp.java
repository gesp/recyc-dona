package com.jianye.recycdona.control.http;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.jianye.recycdona.control.Apps;
import com.jianye.recycdona.control.bean.SMS;
import com.jianye.recycdona.model.fragment.find.bean.Comment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.ContentValues.TAG;


/**
 * 评论请求
 */
public class CommentHttp {

    private static final CommentHttp commentHttp =new CommentHttp();

    private final Gson gson=common.getGson();

    public static String getCodeStatus() {
        return codeStatus;
    }

    public static  String codeStatus;
    private    static final OkHttpClient client= common.getClient();
    private List<Comment.DataBean> contentBeans;

    List<SMS.ContentBean> smsList;



    public static CommentHttp getMyOkHttp() {
        return commentHttp;
    }

    private CommentHttp() {
    }


    /**
     * 请求评论
     * @param findId
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public List<Comment.DataBean> getCommentInfo(String findId) throws ExecutionException, InterruptedException {
        contentBeans=new ArrayList<>();
        CommentInfoHttp callableThread = new CommentInfoHttp(findId);
        FutureTask futureTask = new FutureTask<>(callableThread);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
        return contentBeans;
    }

    class CommentInfoHttp implements Callable {

        String findId;
        public CommentInfoHttp(String findId) {
            this.findId = findId;
        }

        @Override
        public Object call() throws Exception {

            Request request = new Request.Builder()
                    .url(Apps.getCommentInfo+findId)//请求接口。如果需要传参拼接到接口后面。
                    .build();//创建Request 对象
            Response response = null;
            response = client.newCall(request).execute();//得到Response 对象

            Log.d(TAG, "Commeturl: "+Apps.getCommentInfo+findId);
            if (response.isSuccessful()) {

                try {
                    assert response.body() != null;
                    String msginfo = response.body().string();

                    Comment fromJson = gson.fromJson(msginfo, Comment.class);

                    codeStatus=fromJson.getStatus()+"";
                    int listsize=fromJson.getData().size();

                    for(int i=0;i<listsize;i++) {
                        contentBeans.add(fromJson.getData().get(i));
                        Log.d("commentData", "commentData: ----------------->"+fromJson.getData().get(i).getFindid());
                    }

                    Log.d("commoditlength", "comData:----------------->"+listsize);



                } catch (IOException | JsonSyntaxException e) {
                    e.printStackTrace();
                }

            }

            return codeStatus;
        }
    }


    /**
     * 提交评论
     */
    public String postCommentInfo(String findidint, String parenidint, String namevarchar, String cmtcontentvarchar,
                                  String thumbsnumint, String cretedatadatetime, String comentinfovarchar) throws ExecutionException, InterruptedException {

        postCommentInfoHttp callableThread = new postCommentInfoHttp(findidint,parenidint,namevarchar,cmtcontentvarchar
        ,thumbsnumint,cretedatadatetime,comentinfovarchar);

        FutureTask futureTask = new FutureTask<>(callableThread);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
        return codeStatus;
    }

    class postCommentInfoHttp implements Callable {

        String findidint;
        String parenidint;
        String namevarchar;
        String cmtcontentvarchar;
        String thumbsnumint;
        String cretedatadatetime;
        String comentinfovarchar;

        public postCommentInfoHttp(String findidint, String parenidint, String namevarchar, String cmtcontentvarchar,
                                   String thumbsnumint, String cretedatadatetime, String comentinfovarchar) {
            this.findidint = findidint;
            this.parenidint = parenidint;
            this.namevarchar = namevarchar;
            this.cmtcontentvarchar = cmtcontentvarchar;
            this.thumbsnumint = thumbsnumint;
            this.cretedatadatetime = cretedatadatetime;
            this.comentinfovarchar = comentinfovarchar;
        }


        @Override
        public Object call() throws Exception {
            System.out.println(Thread.currentThread().getName());

            try {
                //传递json
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("findid", findidint);
                jsonObject.put("parenid", parenidint);
                jsonObject.put("name", namevarchar);
                jsonObject.put("cmtcontent", cmtcontentvarchar);
                jsonObject.put("thumbsnum", thumbsnumint);
                jsonObject.put("cretedata", cretedatadatetime);
                jsonObject.put("comentinfo", comentinfovarchar);

                String callStr = UserOkHttp.post(Apps.postCommentInfo, jsonObject.toString());
                Log.d(TAG, "jsonObject.toString()= "+jsonObject.toString());
                JSONObject call_json = new JSONObject(callStr);
                codeStatus = call_json.getString("status");
                Log.d(TAG, "--------------->call: "+codeStatus);

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            System.out.print("-------------------------------OK");
            return codeStatus;
        }

    }

    /**
     * http://localhost:8083/api/smsapi
     */
    public List<SMS.ContentBean> getsmsapi() throws ExecutionException, InterruptedException {
        smsList=new ArrayList<>();
        SMSHttp callableThread = new SMSHttp();
        FutureTask futureTask = new FutureTask<>(callableThread);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
        return smsList;
    }

    class SMSHttp implements Callable {

        @Override
        public Object call() throws Exception {

            Request request = new Request.Builder()
                    .url(Apps.getsmsApi)//请求接口。如果需要传参拼接到接口后面。
                    .build();//创建Request 对象
            Response response = null;
            response = client.newCall(request).execute();//得到Response 对象
            if (response.isSuccessful()) {

                try {
                    assert response.body() != null;
                    String msginfo = response.body().string();
                    SMS fromJson = gson.fromJson(msginfo, SMS.class);
                    for(int i=0;i<fromJson.getContent().size();i++) {
                        smsList.add(fromJson.getContent().get(i));
                        Log.d("commentData", "commentData: ----------------->"+fromJson.getContent().get(i).getUrl());
                    }
                } catch (IOException | JsonSyntaxException e) {
                    e.printStackTrace();
                }

            }

            return  codeStatus;
        }
    }

}
