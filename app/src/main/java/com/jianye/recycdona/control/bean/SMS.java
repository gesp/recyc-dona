package com.jianye.recycdona.control.bean;

import java.util.List;


public class SMS {
    private List<ContentBean> content;
    private Integer totalElements;

    public List<ContentBean> getContent() {
        return content;
    }

    public void setContent(List<ContentBean> content) {
        this.content = content;
    }

    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }


    public static class ContentBean {
        private String acount;
        private Integer id;
        private String secretkey;
        private String template;
        private String url;

        public String getAcount() {
            return acount;
        }

        public void setAcount(String acount) {
            this.acount = acount;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getSecretkey() {
            return secretkey;
        }

        public void setSecretkey(String secretkey) {
            this.secretkey = secretkey;
        }

        public String getTemplate() {
            return template;
        }

        public void setTemplate(String template) {
            this.template = template;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
