package com.jianye.recycdona.control;

import com.jianye.recycdona.control.bean.SMS;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;

public class Apps {

    static List<SMS.ContentBean> listsms=new ArrayList<>();

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    //短信api
//    public final static String APISMS="https://yiketianqi.com/api/sms?appid=45121489&appsecret=96nzoUYF";

//    public static String APISMS = null;

//    static {
//        try {
//
//            listsms= CommentHttp.getMyOkHttp().getsmsapi();
//            String acoun= listsms.get(0).getAcount();
//            String url=listsms.get(0).getUrl();
//            String secretkey=listsms.get(0).getSecretkey();
//            String template=listsms.get(0).getTemplate();
//            APISMS =url+"?"+"appid="+acoun+"&appsecret="+secretkey+"&template_id="+template;
//        } catch (ExecutionException | InterruptedException e) {
//            e.printStackTrace();
//        }
//    }

    //检查手机号是否已经注册提交到后台
    public final static String findbyphone="http://106.15.74.249/api/recyclerUser/findbyphone";

    //第一次注册，将手机号存入后台数据库
    public final static String adduser="http://106.15.74.249/api/recyclerUser/adduser";

    //查询用户信息
    public final static String selectuserinfo="http://106.15.74.249/api/recyclerUser/selectphonelist";

    //修改用户信息
    public final static String updateUserInfo="http://106.15.74.249/api/recyclerUser/updateUserInfo";

    /*************addresss**************/
    //post
    public final static String PostReserveInfo="http://106.15.74.249/api/recyclerAddress/addressinfo";
    //get
    public final static String GetReserveInfo="http://106.15.74.249/api/recyclerAddress/selectAddressinfo";

    //查询parent_id=用户id的所有地址
    public final static String UpdateReserveInfo="http://106.15.74.249/api/recyclerAddress/updateUserInfo";
    // {"parent_id":"15","receiver":"刘四","phone":"123456789","address":"天津","detai_address":"美团美团"}
   //根据id删除
    public final static String DeleteReserveInfo=" http://106.15.74.249/api/recyclerAddress/deleteAddressInfo/";

    //根据id查询
    public final static String findIdAddressinfo=" http://106.15.74.249/api/recyclerAddress/findIdAddressinfo/";


    /****ORDER****/

    public final static String postOrderInfo="http://106.15.74.249/api/recyclerReserve/postOrderInfo";
  //  public final static String deleteOrderInfo="http://106.15.74.249/api/recyclerAddress/findIdAddressinfo/";

    public final static String putOrderInfo="http://106.15.74.249/api/recyclerReserve/putOrderInfo";

    public final static String getOrderInfo=" http://106.15.74.249/api/recyclerReserve/getOrderInfo/";

    //轮播接口
    public final static String getCarousel="http://106.15.74.249/api/recyclerCarousel";

    //获取积分数据
    public final static String getCredits="http://106.15.74.249/api/recyclerCredits";

    //获取商品信息
    public final static String getcommodityManage="http://106.15.74.249/api/commodityManage";

    //提交订单到数据库
    public final static String postCommdityOrder="http://106.15.74.249/api/commodityOrder/postCommdityOrder";

    //查询用户订单
    public final static String getCommdityOrder="http://106.15.74.249/api/commodityOrder/getCommdityOrder/";

    //修改订单
    public final static String putCommdityOrder="http://106.15.74.249/api/commodityOrder/putCommdityOrder";
    //删除订单
    public final static String deleteCommdityOrder="http://106.15.74.249/api/commodityOrder/deleteCommdityOrder/";

    public final static String getrecyclerFind="http://106.15.74.249/api/recyclerFind";

    //评论
    public final static String getCommentInfo="http://106.15.74.249/api/comment/getCommentInfo/";
    public final static String postCommentInfo="http://106.15.74.249/api/comment/postCommentInfo";

    //sms
    public final static String getsmsApi="http://106.15.74.249/api/smsapi";


    //提交商品反馈信息
    public final static String postShopCommentComment="http://106.15.74.249/api/shopcomment/postShopCommentComment";



}
