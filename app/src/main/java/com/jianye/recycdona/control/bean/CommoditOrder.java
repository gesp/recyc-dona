package com.jianye.recycdona.control.bean;

import java.util.List;


public class CommoditOrder {


    private List<ContentBean> content;
    private Integer totalElements;

    public List<ContentBean> getContent() {
        return content;
    }

    public void setContent(List<ContentBean> content) {
        this.content = content;
    }

    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }


    public static class ContentBean {
        private String categories;
        private String commcarousel;
        private String detailsMap;
        private String freigh;
        private Integer id;
        private Integer insock;
        private Integer pintsRedeem;
        private Double price;
        private String produconver;
        private String productName;
        private Integer productSold;
        private String produdetails;
        private String specifi;
        private String status;

        public String getCategories() {
            return categories;
        }

        public void setCategories(String categories) {
            this.categories = categories;
        }

        public String getCommcarousel() {
            return commcarousel;
        }

        public void setCommcarousel(String commcarousel) {
            this.commcarousel = commcarousel;
        }

        public String getDetailsMap() {
            return detailsMap;
        }

        public void setDetailsMap(String detailsMap) {
            this.detailsMap = detailsMap;
        }

        public String getFreigh() {
            return freigh;
        }

        public void setFreigh(String freigh) {
            this.freigh = freigh;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getInsock() {
            return insock;
        }

        public void setInsock(Integer insock) {
            this.insock = insock;
        }

        public Integer getPintsRedeem() {
            return pintsRedeem;
        }

        public void setPintsRedeem(Integer pintsRedeem) {
            this.pintsRedeem = pintsRedeem;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public String getProduconver() {
            return produconver;
        }

        public void setProduconver(String produconver) {
            this.produconver = produconver;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public Integer getProductSold() {
            return productSold;
        }

        public void setProductSold(Integer productSold) {
            this.productSold = productSold;
        }

        public String getProdudetails() {
            return produdetails;
        }

        public void setProdudetails(String produdetails) {
            this.produdetails = produdetails;
        }

        public String getSpecifi() {
            return specifi;
        }

        public void setSpecifi(String specifi) {
            this.specifi = specifi;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
