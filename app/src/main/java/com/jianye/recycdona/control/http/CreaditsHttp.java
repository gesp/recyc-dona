package com.jianye.recycdona.control.http;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.jianye.recycdona.control.Apps;
import com.jianye.recycdona.control.bean.Credits;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CreaditsHttp {
    private static final CreaditsHttp creaditsHttp=new CreaditsHttp();

    private final Gson gson=common.getGson();
    public static  String codeStatus;
    private    static final OkHttpClient client= common.getClient();
    private List<Credits.ContentBean> creditsList;

    public static CreaditsHttp getMyOkHttp() {
        return creaditsHttp;
    }

    private CreaditsHttp() {
    }


    /**
     * Get 获取所有积分数据
     */
    public List<Credits.ContentBean> getCredits() throws ExecutionException, InterruptedException {
        creditsList=new ArrayList<>();
        CreditsHttp callableThread = new CreditsHttp();
        FutureTask futureTask = new FutureTask<>(callableThread);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
        return creditsList;
    }

    class CreditsHttp implements Callable {

        @Override
        public Object call() throws Exception {


            Request request = new Request.Builder()
                    .url(Apps.getCredits)//请求接口。如果需要传参拼接到接口后面。
                    .build();//创建Request 对象
            Response response = null;
            response = client.newCall(request).execute();//得到Response 对象

            if (response.isSuccessful()) {

                try {
                    assert response.body() != null;
                    String msginfo = response.body().string();

                    Credits fromJson = gson.fromJson(msginfo, Credits.class);

                    int listsize=fromJson.getContent().size();

                    for(int i=0;i<listsize;i++) {
                        creditsList.add(fromJson.getContent().get(i));
                    }
                    Log.d("creditsleng", "call: ----------------->"+listsize);



                } catch (IOException | JsonSyntaxException e) {
                    e.printStackTrace();
                }

            }

            return 200;
        }
    }

    /**
     * 查询当前用户积分
     */


}
