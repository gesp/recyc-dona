package com.jianye.recycdona.control.bean;

import java.util.List;

/**
 *
 * 商品订单
 */
public class ComOrder {
    private List<DataBean> data;
    private String msg;
    private Integer status;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public static class DataBean {
        private Integer addressId;
        private String addressinfo;
        private Integer commid;
        private String createDate;
        private String freight;
        private Integer id;
        private String nunber;
        private String orderinfo;
        private String ordernum;
        private Integer paidpoint;
        private Integer parentId;
        private Double price;
        private String status;

        public Integer getAddressId() {
            return addressId;
        }

        public void setAddressId(Integer addressId) {
            this.addressId = addressId;
        }

        public String getAddressinfo() {
            return addressinfo;
        }

        public void setAddressinfo(String addressinfo) {
            this.addressinfo = addressinfo;
        }

        public Integer getCommid() {
            return commid;
        }

        public void setCommid(Integer commid) {
            this.commid = commid;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getFreight() {
            return freight;
        }

        public void setFreight(String freight) {
            this.freight = freight;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getNunber() {
            return nunber;
        }

        public void setNunber(String nunber) {
            this.nunber = nunber;
        }

        public String getOrderinfo() {
            return orderinfo;
        }

        public void setOrderinfo(String orderinfo) {
            this.orderinfo = orderinfo;
        }

        public String getOrdernum() {
            return ordernum;
        }

        public void setOrdernum(String ordernum) {
            this.ordernum = ordernum;
        }

        public Integer getPaidpoint() {
            return paidpoint;
        }

        public void setPaidpoint(Integer paidpoint) {
            this.paidpoint = paidpoint;
        }

        public Integer getParentId() {
            return parentId;
        }

        public void setParentId(Integer parentId) {
            this.parentId = parentId;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
